#Prepare Martserver/Occiware runtime configuration
FROM alpine:3.5 as git
RUN apk update \
    && apk add wget \
    && apk add unzip \
    && mkdir /mart \
    && mkdir /examples \
    && mkdir /examples/imports \
    && mkdir /examples/imports/smartshark \
    && mkdir /examples/imports/mls \
    && mkdir /examples/imports/hadoop \
    && mkdir /examples/imports/deployment-examples \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/raw/master/org.occiware.mart.jetty.jar -O /mart/martserver.jar \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup/-/archive/master/de.ugoe.cs.rwm.mart.setup-master.zip?path=martserver-plugins -O plugins.zip \
    && unzip plugins.zip \
    && mv de.ugoe.cs.rwm.mart.setup-master-martserver-plugins/martserver-plugins/ /mart \
    #Download example models
    #Hadoop
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/hadoop/wordcount_workflow_distr_fetcher.occic?inline=false -O  /examples/imports/hadoop/wordcount_workflow_distr_fetcher.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/hadoop/wordcount_workflow.occic?inline=false -O  /examples/imports/hadoop/wordcount_workflow.occic \
    #Simulation
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/mls/mlsDecision.occic?inline=false -O  /examples/imports/mls/mlsDecision.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/mls/mlsManual.occic?inline=false -O  /examples/imports/mls/mlsManual.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/mls/mlsLoop.occic?inline=false -O  /examples/imports/mls/mlsLoop.occic \
    #Smartshark
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/smartshark/shark_base.occic?inline=false -O  /examples/imports/smartshark/shark_base.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/smartshark/shark_par3.occic?inline=false -O  /examples/imports/smartshark/shark_par3.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/wocci/smartshark/shark_par3_noPlat.occic?inline=false -O  /examples/imports/smartshark/shark_par_noPlat.occic \
    #Deployment
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/docci/Multi-Tier-Deployment/Multi-Tier-Deployment.occic?inline=false -O  /examples/imports/deployment-examples/Multi-Tier-Deployment.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/docci/JupyterNotebook/notebook.occic?inline=false -O  /examples/imports/deployment-examples/jupyter-notebook.occic \
    && wget --no-check-certificate https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples/-/raw/master/docci/Wordpress/wordpress-topology.extendedTosca?inline=false -O  /examples/imports/deployment-examples/wordpress-topology.occic


#Build SmartWYRM
FROM gradle:4.7.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN mv build-ci.gradle build.gradle \
    && mv settings.gradle settings-ci.gradle
RUN gradle assemble

#Create Runtime Container
FROM openjdk:8-jre-slim
EXPOSE 8080
EXPOSE 8081

RUN mkdir /martserver-plugins \
    && mkdir /apps \
    && mkdir /root/.rwm \
    && mkdir /root/.rwm/store

COPY --from=build /home/gradle/src/build/libs/smartwyrm.jar /smartwyrm.jar
COPY --from=git /mart/martserver.jar /martserver.jar
COPY --from=git /mart/martserver-plugins /martserver-plugins
COPY --from=git /examples /root/.rwm/store
COPY dockerstart.sh /root/dockerstart.sh

ENTRYPOINT ["sh", "/root/dockerstart.sh"]