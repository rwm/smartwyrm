## SmartWYRM
<img src="src/main/resources/public/logo/wyrm.png" align="right" width = "200px">
Smart Workflows through dYnamic Runtime Models (SmartWYRM) is a server application that allows to visualize orchestrate models designed with the Open Cloud Computing Interface (OCCI) cloud standard.
SmartWYRM provides several core functionalities to orchestrate cloud deployments as well as execute scientific workflows designed with OCCI.
Currently, the webserver is implemented to communicate with the MartServer aka [OCCIWare Runtime Server](https://github.com/occiware/MartServer), a middleware server translating incoming OCCI requests to a provider specific interfaces.
Within this project different sets of connectors for the OCCIWare Runtime Server where used. One connector set is used for simulation and testing purposes while the other one provide a direct connection to an Openstack Cloud.
A description of how to get started with SmartWYRM can be found in the following.

### Getting Started
To get started with SmartWYRM we highly recommend the utilization of the provided Docker image as it provides a preconfigured version of SmartWYRM, the OCCIWare Runtime.
In the following a getting started video and the required steps for its reproduction are provided.

#### 1. Starting SmartWYRM
1. Download the SmartWYRM docker image:
    ```
     docker pull erbel/smartwyrm:1.0.1
   ```
2. Start the image:
   ```
   docker run -p 8080:8080 -p 8081:8081 erbel/smartwyrm:1.0.1
   ```
3. Now SmartWYRM is reachable at [localhost:8081](localhost:8081)
    
#### 2. Executing an example Workflow
1. Select an example from the [Model Store](localhost:8081/rocci):
    1. Click on the three circles in the top right corner of the box.
    2. Select choose as deployment to deploy the model.
    3. Select choose as workflow to execute the modelled workflow.
    4. Based on your decision you are navigated to either the DOCCI or WOCCI site.
2. Finally, press on transform/start to trigger the corresponding engine.
3. Watch how the engine adpapts the runtime model.
4. *Optional:* If no cleanup step is provided, you can empty the runtime mode manually.
    1. Click on the three dots in the top right corner of the runtime model box.
    2. Choose "Empty Runtime"
5. *Optional:* To get further information about the job you can have a look at the [Job History](localhost:8081/history).

*Note:* During the execution of component actions are simulated ranging from 2 to 5 seconds to allow for a better visualisation of the process. A description as well as a video demonstrating the examples in a cloud environment can be found [here](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples).

![MLS Manual](doc/img/gettingStarted(Edited).mp4)


### Functionalities & Features
SmartWYRM provides several functionalities to work with OCCI models and inspect how they change at runtime.
To get more information about a specific feature click on one of the following links:
1. [WOCCI/DOCCI Engine](/doc/engine.md): A workflow execution and cloud adaptation engine utilizing a OCCI workflow extension.
2. [Runtime View](/doc/runtime.md): The visualisation of the runtime model and according features.
3. [Job History](/doc/history.md): A detailed visualisation of previous job executions.
4. [Model Store](/doc/store.md): Stores previous executed models and allows to automatically import models (.occic files) from git projects.
5. [Screenshot](/doc/screenshot.md): Creation of high resolution screenshots from the runtime model with few editing options.

### Settings & Configurations
A more precise description of how to configure the MartServer including information for a connection to a cloud environment can be found [here](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.mart.setup).

