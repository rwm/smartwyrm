# Engines

SmartWYRM serves as wrapper for two engines:
1. [WOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.wocci) is a workflow execution engine based on the OCCI standard.
2. [DOCCI](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.docci) is an adaptation engine based on the OCCI standard.

For both engines, SmartWYRM provides a visual interface to upload input models,
visualizes them as well as changing runtime states.
The shown figure depicts the typical user interface for both engines.
A more detailed description of these sections is given in the following.

## 1: Options/Functions
The typical options provided by the engines are the following:
1. Browse: Choose the model serving as input for the engine.
2. Upload: Upload the chosen model. After the upload it gets visualized.
3. Transform: Performs a model transformation on the model which, e.g., adds a management network to the model as well as some default values.
4. Start/Enact: Triggers the execution of the engine. While the engine is running the Runtime Model Visualisation is periodically refreshed.
5. Stop: Stops the execution of the engine.

## 2: Design Time Model Visualisation
In this box the visualisation of the uploaded design time model is shown. In this case a workflow is visualized.
A description of the icons can be found in the legends next to the runtime model visualisation.

## 3: Runtime Model Visualisation
This box visualiszed the current state of the runtime model. Currently, the visualisation is refreshed, when the site is refreshed
or while one of the engines is running. Refreshing the page while an engine is running stops the periodic update.
In this case the dashboard can be opened always refreshing the runtime model.

You can find further information about the runtime model visualisation [here](/doc/runtime.md).

<img src="doc/img/engine.png" width = "900px">

