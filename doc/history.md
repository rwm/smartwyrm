# Job History
For each triggered engine execution a history is recorded providing information about how long individual requests took.
Furthermore, the current and desired runtime state of the engines are stored, compared and visualized providing feedback about
the engines behaviour. For both the DOCCI and WOCCI jobs different views are provided. 
Independent of the engine the start date, end date, duration and status of a job are displayed.

## WOCCI Job History
The WOCCI job history view provides the following information:
1. The execution time of a task,
2. the time required by the scheduling process and
3. a timeline showing when the individual components of the workflow engine took place.

It should be noted that the bars in the scheduling details plot can be clicked to get further information about the adaptation process.
This opens the DOCCI job history view explained in the following.

<img src="doc/img/wocciHist.png" width = "900px">

## DOCCI Job History
The DOCCI job history view is quite similar to the WOCCI view.
However, it provides different kind of information:
1. The request time for CRUD requests:
   1. Deprovisioning
   2. Provisioning
   3. Deployment
2. A comparison between the current and desired model state
3. A timeline showing when the different requests took place
4. A comparison of the duration between the individual adaptation steps

<img src="doc/img/docciHist.png" width = "900px">
