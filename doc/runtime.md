# Runtime Model Visualisation
The visualisation of the runtime model allows to interact with the system, e.g., by adjusting control flows or single attributes of entities.
For the visualisation of the model the [vis.js](https://visjs.org/) framework wis used.

## 1 Runtime Model
Here, the current state of the runtime model is shown. This box provides several functionalities related to
the depicted model elments. Typically, next to the runtime model is a legend with icons used in the model.
Hovering over these icons provides more information about the provided functionalities of the model.
This legend is in the shown figure ommited as it replaced with the generic entity information.

Overall, the runtime model provides the following functionality:

1. Clicking on an entity opens its Entity Generics (3) view showing its attributes and actions.
2. Double-Clicking an entity opens the Entity Specifics view:
3. Clicking the Edit button resources and edges can be added to the runtime model.
    1. A dialog is popped up navigating through the creation process based on registered OCCI extensions.    
4. On the top right corner of the runtime model box the three dots can be clicked to get the following options:
    1. Empty Runtime Model: Deletes the current runtime model (except the management network).
    2. Export Portable Runtime Model: Downloads a portable version of the current runtime model (removes runtime specific information)
    3. Export Exact Runtime Model: Downloads the model as is (mainly used for testing purposes)

## 2 Entity Specifics
This view is opened as soon as an entity is double clicked. Depending on the kind of resource clicked another view is opened.
For example:
1. Components show their corresponding configuration management script in an editor.
2. Compute nodes show their init script in an editor.
3. Tasks and applications collapses with their corresponding components.

*Note:* Depicted scripts can be edited and saved with Strg+s allowing to test configuration management scripts on the fly.

## 3 Entity Generics
The Entity Generics view is opened as soon as a runtime entity is clicked.
Thereafter, the legend is hidden and the attributes and actions of the entity are displayed.
The action are hereby derived from registered OCCI extensions.
This view offers the following options:
 
1. Trigger an action by clicking on the corresponding button.
2. Click on the button the left of an attribute to change the attribute.


<img src="doc/img/runtime.png" width = "900px">