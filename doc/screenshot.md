# Screenshot
The screenshot side allows to export a high resolution png of the currently deployed runtime model.
Furthermore, it provides some functionality to directly affect the visualisation of the model.