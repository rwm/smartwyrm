# Model Store
The model stores a set of OCCI models previously uploaded. 
Furthermore, it allows to import .occic models directly form git repositories.

<img src="doc/img/store.png" align="right" width = "300px">
The stored models can be directly chosen by pressing clicking on the three circles in the top right corner of a model visualisation.
Three options are available:

1. Select choose as deployment to deploy the model,
2. Select choose as workflow to execute the modelled workflow and
3. Delete the stored model.

Based on the option chosen the browser navigates to the corresponding engine.
