/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm;

import java.nio.file.Path;
import java.nio.file.Paths;

public class GlobalRegistry {

	private Path runtimepath = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private String store = System.getProperty("user.home") + "/.rwm/store/";
	private String jobHistoryFolder = System.getProperty("user.home") + "/.rwm/job_history/";
	private SettingsHelper helper = new SettingsHelper();
	private String user = helper.getProperties().getProperty("user");
	private String address = helper.getProperties().getProperty("address");
	private int port = Integer.parseInt(helper.getProperties().getProperty("port"));
	private int sleep = Integer.parseInt(helper.getProperties().getProperty("sleep"));
	private String ansibleRoles = helper.getProperties().getProperty("ansibleRoles");

	private String manNWid = helper.getProperties().getProperty("manNWid");
	private String manNWRuntimeId = helper.getProperties().getProperty("manNWRuntimeId");
	private String sshKey = helper.getProperties().getProperty("sshKey");
	private String userData = helper.getProperties().getProperty("userData");
	private String flavor = helper.getProperties().getProperty("flavor");
	private String image = helper.getProperties().getProperty("image");
	private String remoteUser = helper.getProperties().getProperty("remoteUser");

	private String archScheduler = helper.getProperties().getProperty("archScheduler");
	private String performer = helper.getProperties().getProperty("performer");
	private String pluginsFolder = helper.getProperties().getProperty("pluginsFolder");

	private String privateKey = helper.getProperties().getProperty("privateKey");

	public String getArchScheduler() {
		return archScheduler;
	}

	public void setArchScheduler(String archScheduler) {
		this.archScheduler = archScheduler;
	}

	public String getPluginsFolder() {
		return pluginsFolder;
	}

	public void setPluginsFolder(String pluginsFolder) {
		this.pluginsFolder = pluginsFolder;
	}

	public String getFlavor() {
		return flavor;
	}

	public void setFlavor(String flavor) {
		this.flavor = flavor;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getRemoteUser() {
		return remoteUser;
	}

	public void setRemoteUser(String remoteUser) {
		this.remoteUser = remoteUser;
	}

	private int wocciCycle = Integer.parseInt(helper.getProperties().getProperty("wocciCycle"));

	public int getWocciCycle() {
		return wocciCycle;
	}

	public void setWocciCycle(int wocciCycle) {
		this.wocciCycle = wocciCycle;
	}

	private static GlobalRegistry instance = new GlobalRegistry();

	private GlobalRegistry() {
	}

	public static GlobalRegistry getInstance() {
		return instance;
	}

	public java.nio.file.Path getRuntimePath() {
		return runtimepath;
	}

	public void setRUNTIMEPATH(java.nio.file.Path runtimepath) {
		this.runtimepath = runtimepath;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getSleep() {
		return sleep;
	}

	public void setSleep(int sleep) {
		this.sleep = sleep;
	}

	public String getAnsibleRoles() {
		return ansibleRoles;
	}

	public void setAnsibleRoles(String ansibleRoles) {
		this.ansibleRoles = ansibleRoles;
	}

	public String getManNWid() {
		return manNWid;
	}

	public void setManNWid(String manNWid) {
		this.manNWid = manNWid;
	}

	public String getManNWRuntimeId() {
		return manNWRuntimeId;
	}

	public void setManNWRuntimeId(String manNWRuntimeId) {
		this.manNWRuntimeId = manNWRuntimeId;
	}

	public String getSshKey() {
		return sshKey;
	}

	public void setSshKey(String sshKey) {
		this.sshKey = sshKey;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getJobHistoryFolder() {
		return jobHistoryFolder;
	}

    public String getPerformer() {
		return performer;
    }

    public String getPrivateKey() {
		return privateKey;
    }

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}

	public void setPerformer(String performer) {
		this.performer = performer;
	}
}
