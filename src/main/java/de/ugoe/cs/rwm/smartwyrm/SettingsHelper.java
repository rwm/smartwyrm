package de.ugoe.cs.rwm.smartwyrm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class SettingsHelper {
	Properties props;
	private final static String FILENAME = "settings.properties";
	private final static String SETTINGDIRNAME = System.getProperty("user.home") + "/.rwm/settings/";

	public SettingsHelper() {
		loadProperties();
	}

	/**
	 * Getter method for providing the properties of this SettingsHelper. Properties
	 * will be read from local file settings.properties.
	 *
	 * @return The properties
	 */
	public Properties getProperties() {
		if (props == null) {
			loadProperties();
		}

		return props;
	}

	private void loadProperties() {
		props = new Properties();
		InputStream input = null;
		if ((new File(SETTINGDIRNAME)).exists()) {
			File settings = new File(SETTINGDIRNAME + FILENAME);
			if (settings.exists()) {
				try {
					input = new FileInputStream(settings);
					props.load(input);
					System.out.println("Loaded properties from: " + settings.toString());
				} catch (IOException e) {
					loadDefaultProperties();
				} finally {
					if (input != null) {
						try {
							input.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		} else {
			loadDefaultProperties();
		}
	}

	private void loadDefaultProperties() {
		InputStream input = null;
		try {
			input = this.getClass().getClassLoader().getResourceAsStream(FILENAME);
			props.load(input);
			System.out.println("Loaded properties from: DEFAULT");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void storeProperties() {
		File settingDir = new File(SETTINGDIRNAME);
		if (!settingDir.exists()) {
			boolean mkdir = settingDir.mkdirs();
			if (mkdir) {
				System.out.println("Settingsfolder created");
			}
		}
		File file = new File(SETTINGDIRNAME + FILENAME);
		GlobalRegistry reg = GlobalRegistry.getInstance();
		props.setProperty("user", reg.getUser());
		props.setProperty("address", reg.getAddress());
		props.setProperty("port", String.valueOf(reg.getPort()));
		props.setProperty("sleep", String.valueOf(reg.getSleep()));
		props.setProperty("ansibleRoles", reg.getAnsibleRoles());
		props.setProperty("manNWid", reg.getManNWid());
		props.setProperty("manNWRuntimeId", reg.getManNWRuntimeId());
		props.setProperty("sshKey", reg.getSshKey());
		props.setProperty("userData", reg.getUserData());
		props.setProperty("wocciCycle", String.valueOf(reg.getWocciCycle()));
		props.setProperty("flavor", reg.getFlavor());
		props.setProperty("image", reg.getImage());
		props.setProperty("remoteUser", reg.getRemoteUser());
		props.setProperty("pluginsFolder", reg.getPluginsFolder());
		props.setProperty("archScheduler", reg.getArchScheduler());
		props.setProperty("privateKey", reg.getPrivateKey());
		props.setProperty("performer", reg.getPerformer());

		try (FileOutputStream fileOut = new FileOutputStream(file)) {

			props.store(fileOut, "Latest Configuration for the WOCCI Server");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
