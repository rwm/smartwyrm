/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import de.ugoe.cs.rwm.docci.deprovisioner.Deprovisioner;
import jdk.nashorn.internal.objects.Global;
import org.eclipse.cmf.occi.core.*;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.executor.MartExecutor;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;

import java.util.List;

@Controller
public class ActionController {

	@PostMapping("/action")
	public String upload(@RequestParam("node") String id, @RequestParam("action") String action) {
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		conn.loadRuntimeModel(GlobalRegistry.getInstance().getRuntimePath());

		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Entity ent = getEntity(id, (Configuration) occimodel.getContents().get(0));
		MartExecutor exec = new MartExecutor(conn);

		exec.executeOperation("POST", ent, ModelUtility.getAction(ent, action));

		return "redirect:/wocci";
	}

	@PostMapping("/put")
	public String put(@RequestParam("node") String id, @RequestParam("attribute") String attribute,
			@RequestParam("value") String value) {
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		conn.loadRuntimeModel(GlobalRegistry.getInstance().getRuntimePath());

		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Entity ent = getEntity(id, (Configuration) occimodel.getContents().get(0));

		if (attribute.equals("occi.core.title")) {
			ent.setTitle(value);
		} else {
			for (AttributeState attr : ent.getAttributes()) {
				if (attr.getName().equals(attribute)) {
					attr.setValue(value);
				}
			}
			for(MixinBase part: ent.getParts()){
				for (AttributeState attr : part.getAttributes()) {
					if (attr.getName().equals(attribute)) {
						attr.setValue(value);
					}
				}
			}
		}
		//Currently required due to excessive message strings of ansible
		if(!attribute.contains("message")) {
			removeMessageAttributeValue(ent);
		}
		MartExecutor exec = new MartExecutor(conn);

		exec.executeOperation("PUT", ent, null);

		return "redirect:/wocci";
	}

	private void removeMessageAttributeValue(Entity ent) {
		List<AttributeState> toDelete = new BasicEList<>();
		for(AttributeState attr: ent.getAttributes()){
			if(attr.getName().contains("message")){
				toDelete.add(attr);
			}
		}
		EcoreUtil.deleteAll(toDelete, true);
	}

	@PostMapping("/delete")
	public String delete(@RequestParam("entity") String id) {
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		conn.loadRuntimeModel(GlobalRegistry.getInstance().getRuntimePath());

		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Entity ent = getEntity(id, (Configuration) occimodel.getContents().get(0));
		EList<EObject> elist = new BasicEList<EObject>();
		elist.add(ent);
		if(ent instanceof org.eclipse.cmf.occi.core.Resource){
			org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) ent;
			for(Link link:  res.getLinks()){
				elist.add(link);
			}
			for(Link link: res.getRlinks()){
				elist.add(link);
			}
		}
		for(EObject obj: elist){
			System.out.println(obj);
		}

		MartExecutor exec = new MartExecutor(conn);
		Deprovisioner dep = new Deprovisioner(exec, GlobalRegistry.getInstance().getManNWid());
		dep.deprovision(elist);
		//exec.executeOperation("DELETE", ent, null);

		return "redirect:/wocci";
	}

	@PostMapping("/get")
	public String get(@RequestParam("entity") String id) {
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		conn.loadRuntimeModel(GlobalRegistry.getInstance().getRuntimePath());

		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Entity ent = getEntity(id, (Configuration) occimodel.getContents().get(0));

		MartExecutor exec = new MartExecutor(conn);
		exec.executeOperation("GET", ent, null);

		return "redirect:/wocci";
	}

	private Entity getEntity(String id, Configuration configuration) {
		for (org.eclipse.cmf.occi.core.Resource res : configuration.getResources()) {
			if (res.getId().equals(id)) {
				return res;
			}
			for (Link link : res.getLinks()) {
				if (link.getId().equals(id)) {
					return link;
				}
			}
		}
		return null;
	}

}