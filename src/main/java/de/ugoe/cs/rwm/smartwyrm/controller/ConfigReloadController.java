/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import de.ugoe.cs.rwm.smartwyrm.jetty.LifecycleContextListener;

@Controller
public class ConfigReloadController {

	@PostMapping("/reload")
	public String postConfig() {

		try {
			LifecycleContextListener.addPluginsToClassPath();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		org.eclipse.cmf.occi.mart.MART.initMART();

		System.out.println();
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
		return "redirect:/config";
	}
}