/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.connector.MartConnector;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.view.Edge;
import de.ugoe.cs.rwm.smartwyrm.view.Node;
import de.ugoe.cs.rwm.smartwyrm.view.VisUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;

@Controller
public class DocciController {
	private static final Path LOCAL_RUNTIMEMODEL = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private Resource designtimemodel;
	private Path uploadedFile;

	private String manNWid = GlobalRegistry.getInstance().getManNWid();
	private String manNWRuntimeId = GlobalRegistry.getInstance().getManNWRuntimeId();
	private String sshKey = GlobalRegistry.getInstance().getSshKey();
	private String userData = GlobalRegistry.getInstance().getUserData();
	private String address = GlobalRegistry.getInstance().getAddress();
	private String user = GlobalRegistry.getInstance().getUser();
	private int port = GlobalRegistry.getInstance().getPort();
	private String privateKey = GlobalRegistry.getInstance().getPrivateKey();
	private Connector conn;

	private DocciController() {
		if (address.equals("localhost")) {
			this.conn = new LocalhostConnector("localhost", port, user);
		} else {
			this.conn = new MartConnector(address, port, user, privateKey);
		}
	}

	public Resource getDesigntimemodel() {
		return designtimemodel;
	}

	public void setDesigntimemodel(Resource designtimemodel) {
		this.designtimemodel = designtimemodel;
	}

	@GetMapping("/docci")
	public String greeting(Model model, @RequestParam(value = "path", required = false) String path) {
		if (path != null) {
			this.uploadedFile = Paths.get(path);
			this.designtimemodel = ModelUtility.loadOCCIintoEMFResource(Paths.get(path));
		}
		// Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");

		ArrayList<Node> designnodes;
		ArrayList<Edge> designedges;
		if (designtimemodel != null) {
			if (ModelUtility.getOCCIConfiguration(designtimemodel) != null) {
				Configuration configdesign = ModelUtility.getOCCIConfiguration(designtimemodel);
				designnodes = VisUtility.createNodes(configdesign.getResources());
				designedges = VisUtility.createEdges(configdesign.getResources());
			} else {
				designnodes = VisUtility.createNodes(ModelUtility.getResources(designtimemodel.getContents()));
				designedges = VisUtility.createEdges(ModelUtility.getResources(designtimemodel.getContents()));
			}
			model.addAttribute("designnodes", designnodes);
			model.addAttribute("designedges", designedges);
		}

		conn.loadRuntimeModel(LOCAL_RUNTIMEMODEL);
		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Configuration config = (Configuration) occimodel.getContents().get(0);
		ArrayList<Node> nodes = VisUtility.createNodes(config.getResources());
		ArrayList<Edge> edges = VisUtility.createEdges(config.getResources());

		model.addAttribute("runnodes", nodes);
		model.addAttribute("runedges", edges);
		// model.addAttribute("extensions",
		// trimmedExtensionStrings(OcciRegistry.getInstance().getRegisteredExtensions()));
		model.addAttribute("extensions", trimmedExtensionStrings(OcciRegistry.getInstance().getRegisteredExtensions()));
		return "docci";
	}

	private Object[] trimmedExtensionStrings(Collection<String> registeredExtensions) {
		List<String> list = new ArrayList<String>();
		for (String str : registeredExtensions) {
			str = str.substring(str.lastIndexOf('/') - 1, str.lastIndexOf('#'));
			list.add(str);
		}
		System.out.println(registeredExtensions);
		return list.toArray();
	}

	@PostMapping("/docci")
	public String upload(Model model, @RequestParam("file") MultipartFile file) throws IOException {
		byte[] bytes = file.getBytes();
		uploadedFile = Paths.get(GlobalRegistry.getInstance().getStore() + file.getOriginalFilename());
		Files.write(uploadedFile, bytes);

		Resource runmodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Configuration config = (Configuration) runmodel.getContents().get(0);
		ArrayList<Node> nodes = VisUtility.createNodes(config.getResources());
		ArrayList<Edge> edges = VisUtility.createEdges(config.getResources());

		ArrayList<Node> designnodes;
		ArrayList<Edge> designedges;

		Resource designmodel = ModelUtility.loadOCCIintoEMFResource(uploadedFile);

		if (ModelUtility.getOCCIConfiguration(designmodel) != null) {
			Configuration configdesign = ModelUtility.getOCCIConfiguration(designmodel);
			designnodes = VisUtility.createNodes(configdesign.getResources());
			designedges = VisUtility.createEdges(configdesign.getResources());
		} else {
			designnodes = VisUtility.createNodes(ModelUtility.getResources(designmodel.getContents()));
			designedges = VisUtility.createEdges(ModelUtility.getResources(designmodel.getContents()));
		}

		this.designtimemodel = designmodel;

		VisUtility.comparisonColorNodes(designtimemodel, designnodes, runmodel, nodes);

		model.addAttribute("runnodes", nodes);
		model.addAttribute("runedges", edges);

		model.addAttribute("designnodes", designnodes);
		model.addAttribute("designedges", designedges);

		return "docci";
	}

	@PostMapping("/docciDeploy")
	public String upload() throws IOException {
		Date startDate = new Date();
		MartDeployer deployer = new MartDeployer(conn, GlobalRegistry.getInstance().getSleep());
		deployer.setJobHistoryPath(GlobalRegistry.getInstance().getJobHistoryFolder());
		deployer.deploy(designtimemodel);
		return "redirect:/docci";
	}

	@PostMapping("/docciStore")
	public String store() throws IOException {
		Path store = Paths.get(GlobalRegistry.getInstance().getStore() + uploadedFile.getFileName());
		Files.move(uploadedFile, store, StandardCopyOption.REPLACE_EXISTING);
		return "redirect:/docci";
	}

	@PostMapping("/occi2openstack")
	public String transform() throws IOException {
		System.out.println(uploadedFile);
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		try {
			trans.transform(designtimemodel, uploadedFile);
			designtimemodel = ModelUtility.loadOCCIintoEMFResource(uploadedFile);
			OCCI2OPENSTACKTransformator trans2 = new OCCI2OPENSTACKTransformator();
			trans2.setTransformationProperties(manNWRuntimeId, sshKey, userData, manNWid,
					GlobalRegistry.getInstance().getFlavor(), GlobalRegistry.getInstance().getImage(),
					GlobalRegistry.getInstance().getRemoteUser());

			try {
				trans2.transform(designtimemodel, uploadedFile);
			} catch (EolRuntimeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			designtimemodel = ModelUtility.loadOCCIintoEMFResource(uploadedFile);
		} catch (EolRuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "redirect:/docci";
	}

	@PostMapping("/empty")
	public String empty(HttpServletRequest request) throws IOException {
		File emptyOcci = getFile("Empty.occic");
		Resource emptyModel = ModelUtility.loadOCCIintoEMFResource(Paths.get(emptyOcci.getAbsolutePath()));
		// Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		MartDeployer deployer = new MartDeployer(conn, GlobalRegistry.getInstance().getSleep());
		deployer.deploy(emptyModel);
		String referer = request.getHeader("Referer");
		return "redirect:" + referer;
		// return "/docci";
	}

	/**
	 * Returns Transformation File, either contained in the Resource folder or
	 * packed jar.
	 *
	 * @param resourceName
	 * @return The transformation file at the specified location.
	 */
	public static File getFile(String resourceName) {
		File file = null;
		try {
			file = File.createTempFile("tempfile", ".tmp");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (file == null) {
			throw new RuntimeException("Error: Transformationfile " + resourceName + " not found!");
		}

		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

		try (InputStream input = classLoader.getResourceAsStream(resourceName);
				OutputStream out = new FileOutputStream(file)) {
			int read;
			int byteArrSize = 1024;
			byte[] bytes = new byte[byteArrSize];

			while ((read = input.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			file.deleteOnExit();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		if (file.exists() == false) {
			throw new RuntimeException("Error: Transformationfile " + resourceName + " not found!");
		}

		System.out.println(file);

		return file;
	}

}