/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.MixinBase;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.User_data;
import org.eclipse.emf.ecore.resource.Resource;
import org.modmacao.occi.platform.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.view.Editor;

@Controller
public class EditorController {

	@GetMapping("/editorContent")
	@ResponseBody
	public Editor ansible(@RequestParam("id") String id) {
		Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		conn.loadRuntimeModel(GlobalRegistry.getInstance().getRuntimePath());

		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		org.eclipse.cmf.occi.core.Resource res = getResource(id, (Configuration) occimodel.getContents().get(0));
		Editor editor = new Editor();

		String content = "Resource type not supported";
		if (res instanceof Component) {
			Component comp = (Component) res;
			boolean foundFile = false;
			for (MixinBase part : comp.getParts()) {
				System.out.println(part + " : " + part.eClass());
				Path path = Paths.get(
						GlobalRegistry.getInstance().getAnsibleRoles() + "/" + getRolePath(part) + "/tasks/main.yml");
				File f = new File(path.toString());
				if (f.exists() && !f.isDirectory()) {
					try {
						content = new String(Files.readAllBytes(path), Charset.defaultCharset());
						editor.setScript(path);
						foundFile = true;
						break;
					} catch (IOException e) {
						if (foundFile == false) {
							content = "File not found! Searched at: " + path;
						}
					}
				}
			}

		} else if (res instanceof Compute) {
			Compute comp = (Compute) res;
			for (MixinBase part : comp.getParts()) {
				if (part instanceof org.eclipse.cmf.occi.infrastructure.User_data) {
					User_data data = (User_data) part;
					content = new String(Base64.getDecoder().decode(data.getOcciComputeUserdata()));
					break;
				}
			}
		}
		editor.setContent(content);
		return editor;
	}

	@PostMapping("/editorContent")
	public String upload(@RequestParam("script") Path script, @RequestParam("content") String content) {
		if (script != null) {
			System.out.println(script);
			System.out.println(content);

			byte[] strToBytes = content.getBytes(Charset.defaultCharset());

			try {
				Files.write(script, strToBytes);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return "redirect:/wocci";
	}

	private org.eclipse.cmf.occi.core.Resource getResource(String id, Configuration configuration) {
		for (org.eclipse.cmf.occi.core.Resource res : configuration.getResources()) {
			if (res.getId().equals(id)) {
				return res;
			}
		}
		return null;
	}

	private String getRolePath(MixinBase mixin) {
		String rolepath = mixin.getMixin().getScheme();
		rolepath = rolepath.replaceAll("http://", "");
		rolepath = rolepath.replaceAll("\\.", "_");
		rolepath = rolepath.replaceAll("#", "");
		rolepath = rolepath + "/" + mixin.getMixin().getName().toLowerCase();
		return rolepath;
	}
}