/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.emf.ecore.resource.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.history.DocciHistory;
import de.ugoe.cs.rwm.docci.history.JobHistory;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.view.Edge;
import de.ugoe.cs.rwm.smartwyrm.view.Node;
import de.ugoe.cs.rwm.smartwyrm.view.VisUtility;
import de.ugoe.cs.rwm.wocci.enactor.history.TaskHistory;
import de.ugoe.cs.rwm.wocci.performer.history.WocciHistory;
import de.ugoe.cs.rwm.wocci.scheduler.history.SchedulerHistory;

@Controller
public class HistoryController {
	private List<JobHistory> lHist;

	@GetMapping("/history")
	public String history(Model model) throws IOException {
		String jobHistoryFolder = GlobalRegistry.getInstance().getJobHistoryFolder();
		lHist = new ArrayList<JobHistory>();
		File jhs = new File(jobHistoryFolder);
		if (!jhs.exists()) {
			System.out.println("Creating History Dir");
			jhs.mkdir();
			return "history";
		}

		for (final File fileEntry : jhs.listFiles()) {
			try {
				File jobJson = getJobJson(fileEntry);
				JobHistory hist = (JobHistory) extractHistory(jobJson, JobHistory.class);
				lHist.add(hist);
			} catch (FileNotFoundException e) {
				System.out.println("Could not find job file for: " + fileEntry);
				WocciHistory hist = new WocciHistory();
				hist.setJobId(fileEntry.getName());
				hist.setStatus("Could not retrieve jobJson");
				lHist.add(hist);
			}
		}

		model.addAttribute("history", lHist);
		return "history";
	}

	@GetMapping("/history/job")
	public String jobHistory(Model model, @RequestParam("jobId") String id) throws IOException {
		File jobFolder = getJobFolder(id);
		File jobJson = getJobJson(jobFolder);
		JobHistory hist = (JobHistory) extractHistory(jobJson, JobHistory.class);
		if (hist.getBook().equals("WOCCI")) {
			WocciHistory wHist = (WocciHistory) extractHistory(jobJson, WocciHistory.class);
			wHist.getTaskHistory().sort(Comparator.comparing(TaskHistory::getStartDate));
			wHist.getSchedulerHistory().sort(Comparator.comparing(SchedulerHistory::getStartDate));
			model.addAttribute("history", wHist);
			return "wocci_history";
		} else {
			DocciHistory dHist = (DocciHistory) extractHistory(jobJson, DocciHistory.class);
			model.addAttribute("history", dHist);
			addVisToDocciModel(model, jobFolder);
			return "docci_history";
		}
	}

	@GetMapping("/history/job/scheduler")
	public String schedulerJobHistory(Model model, @RequestParam("jobId") String jobId,
			@RequestParam("schedulerId") String schedulerId) throws IOException {
		String jobHistoryFolder = GlobalRegistry.getInstance().getJobHistoryFolder();
		String folderPath = jobHistoryFolder + jobId + "/scheduler/" + schedulerId;
		File jobFolder = new File(folderPath);
		File firstDir = getFirstDir(jobFolder);
		File jobJson = getJobJson(firstDir);
		JobHistory hist = (JobHistory) extractHistory(jobJson, JobHistory.class);
		if (hist.getBook().equals("WOCCI")) {
			WocciHistory wHist = (WocciHistory) extractHistory(jobJson, WocciHistory.class);
			model.addAttribute("history", wHist);
			return "wocci_history";
		} else {
			DocciHistory dHist = (DocciHistory) extractHistory(jobJson, DocciHistory.class);
			model.addAttribute("history", dHist);
			addVisToDocciModel(model, firstDir);
			return "docci_history";
		}
	}

	private void addVisToDocciModel(Model model, File firstDir) {
		String provDir = firstDir.getPath();
		Resource designmodel = ModelUtility.loadOCCIintoEMFResource(Paths.get(provDir + "/Target.occic"));
		Configuration configdesign = ModelUtility.getOCCIConfiguration(designmodel);
		ArrayList<Node> designnodes = VisUtility.createNodes(configdesign.getResources());
		ArrayList<Edge> designedges = VisUtility.createEdges(configdesign.getResources());

		Resource runmodel = ModelUtility.loadOCCIintoEMFResource(Paths.get(provDir + "/Run.occic"));
		Configuration runHistory = ModelUtility.getOCCIConfiguration(runmodel);
		ArrayList<Node> runHistoryNodes = VisUtility.createNodes(runHistory.getResources());
		ArrayList<Edge> runHistoryEdges = VisUtility.createEdges(runHistory.getResources());

		model.addAttribute("designnodes", designnodes);
		model.addAttribute("designedges", designedges);

		model.addAttribute("historyRuntimeNodes", runHistoryNodes);
		model.addAttribute("historyRuntimeEdges", runHistoryEdges);

		VisUtility.comparisonColorNodes(designmodel, designnodes, runmodel, runHistoryNodes);
	}

	private File getFirstDir(File jobFolder) {
		for (File f : jobFolder.listFiles()) {
			System.out.println(f);
			if (f.isDirectory()) {
				return f;
			}
		}
		return null;
	}

	private Object extractHistory(File jobJson, Class clazz) throws IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Object hist = objectMapper.readValue(jobJson, clazz);
		return hist;
	}

	private File getJobJson(File jobFolder) throws FileNotFoundException {
		for (final File jobEntry : jobFolder.listFiles()) {
			if (jobEntry.isFile() && jobEntry.getName().equals("job.json")) {
				return jobEntry;
			}
		}
		throw new FileNotFoundException("Could not find job.json in: " + jobFolder.getName());
	}

	private File getJobFolder(String jobId) throws FileNotFoundException {
		String jobHistoryFolder = GlobalRegistry.getInstance().getJobHistoryFolder();
		for (final File fileEntry : new File(jobHistoryFolder).listFiles()) {
			if (fileEntry.isDirectory() && fileEntry.getName().equals(jobId)) {
				return fileEntry;
			}
		}
		throw new FileNotFoundException("Could not find Job: " + jobId);
	}

}