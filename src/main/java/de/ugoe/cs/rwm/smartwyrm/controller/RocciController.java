/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathSuffixFilter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.store.StoreModel;
import de.ugoe.cs.rwm.smartwyrm.store.StoreModelRepository;
import de.ugoe.cs.rwm.smartwyrm.view.Edge;
import de.ugoe.cs.rwm.smartwyrm.view.Node;
import de.ugoe.cs.rwm.smartwyrm.view.VisUtility;

@Controller
public class RocciController {
	private final static String IMPORTDIR = GlobalRegistry.getInstance().getStore() + "/" + "imports/";
	private List<StoreModel> storeModels = new ArrayList<StoreModel>();
	private List<StoreModelRepository> storeModelRepositories = new ArrayList<StoreModelRepository>();

	@GetMapping("/rocci")
	public String greeting(Model model) {
		storeModelRepositories.clear();
		instantiateStoreModels();
		sort(storeModelRepositories);
		// this.storeModels = listFilesForFolder(new
		// File(GlobalRegistry.getInstance().getStore()));
		model.addAttribute("storeModelRepositories", storeModelRepositories);
		return "rocci";
	}

	private void sort(List<StoreModelRepository> storeModelRepositories) {
		int swap = -1;
		for (StoreModelRepository smr : storeModelRepositories) {
			if (smr.getRootDir().equals("hadoop")) {
				swap = storeModelRepositories.indexOf(smr);
			}
		}
		if (swap != -1) {
			Collections.swap(storeModelRepositories, swap, 0);
		}
	}

	private void instantiateStoreModels() {
		instantiateImports();
		instantiateLocal();
	}

	private void instantiateLocal() {
		StoreModelRepository repo = new StoreModelRepository("local");
		List<StoreModel> models = listLocalFiles(new File(GlobalRegistry.getInstance().getStore()));
		repo.getStoreModels().addAll(models);
		System.out.println("Local: " + repo.getStoreModels());
		storeModelRepositories.add(repo);
	}

	private void instantiateImports() {
		File impDir = new File(IMPORTDIR);
		if (!impDir.exists()) {
			System.out.println("Creating Import Dir");
			impDir.mkdir();
		}
		File[] imports = impDir.listFiles();
		for (File file : imports) {
			StoreModelRepository repo = new StoreModelRepository(file.getName());
			System.out.println("REPO NAME: " + file.getName());
			System.out.println(file);
			List<StoreModel> models = listFilesForFolder(file);
			repo.getStoreModels().addAll(models);
			System.out.println("Repo: " + file.getName() + " Models: " + repo.getStoreModels());
			storeModelRepositories.add(repo);
		}
	}

	@PostMapping("/rocci/docci")
	public RedirectView postDocci(@RequestParam("repo") String repo, @RequestParam("model") String model,
			RedirectAttributes attributes) throws FileNotFoundException {
		RedirectView redirect = new RedirectView("/docci");
		attributes.addAttribute("path", getStoreModelPath(repo, model).toString());

		return redirect;
	}

	@PostMapping("/rocci/wocci")
	public RedirectView postWocci(@RequestParam("repo") String repo, @RequestParam("model") String model,
			RedirectAttributes attributes) throws FileNotFoundException {
		RedirectView redirect = new RedirectView("/wocci");
		attributes.addAttribute("path", getStoreModelPath(repo, model).toString());

		return redirect;
	}

	@DeleteMapping("/rocci")
	public String delete(@RequestParam("repo") String repo, @RequestParam("model") String model,
			RedirectAttributes attributes) throws FileNotFoundException {
		File file = new File(getStoreModelPath(repo, model).toString());
		if (file.delete()) {
			System.out.println("Deleted: " + file);
		} else {
			System.out.println("File: " + file + " does not exist");
		}
		return "redirect:/rocci";
	}

	@PostMapping("/rocci/import")
	public String importStoreModels(@RequestParam("uri") String uri) throws GitAPIException, IOException {
		importGit(uri);
		return "redirect:/rocci";
	}

	public List<StoreModel> listFilesForFolder(final File folder) {
		List<StoreModel> models = new ArrayList<StoreModel>();
		for (final File fileEntry : folder.listFiles()) {
			System.out.println(fileEntry);
			if (fileEntry.isDirectory()) {
				models.addAll(listFilesForFolder(fileEntry));
			} else {
				if ("occic".equals(FilenameUtils.getExtension(fileEntry.getName()))) {
					System.out.println(fileEntry.getName());
					try {
						Resource designtimemodel = ModelUtility
								.loadOCCIintoEMFResource(Paths.get(fileEntry.getAbsolutePath()));

						ArrayList<Node> designnodes;
						ArrayList<Edge> designedges;
						if (ModelUtility.getOCCIConfiguration(designtimemodel) != null) {
							Configuration configdesign = ModelUtility.getOCCIConfiguration(designtimemodel);
							designnodes = VisUtility.createNodes(configdesign.getResources());
							designedges = VisUtility.createEdges(configdesign.getResources());
						} else {
							designnodes = VisUtility
									.createNodes(ModelUtility.getResources(designtimemodel.getContents()));
							designedges = VisUtility
									.createEdges(ModelUtility.getResources(designtimemodel.getContents()));
						}

						StoreModel store = new StoreModel(Paths.get(fileEntry.getAbsolutePath()),
								FilenameUtils.getBaseName(fileEntry.getName()));

						store.setNodes(designnodes);
						store.setEdges(designedges);
						System.out.println("Model added: " + store);
						models.add(store);
					} catch (Exception e) {
						System.out.println("OCCI Configuration could not be loaded: " + fileEntry.getName());
					}
				}
			}
		}
		return models;
	}

	public List<StoreModel> listLocalFiles(final File folder) {
		List<StoreModel> models = new ArrayList<StoreModel>();
		for (final File fileEntry : folder.listFiles()) {
			System.out.println(fileEntry);
			if (fileEntry.isDirectory() == false) {
				if ("occic".equals(FilenameUtils.getExtension(fileEntry.getName()))) {
					System.out.println(fileEntry.getName());
					try {
						Resource designtimemodel = ModelUtility
								.loadOCCIintoEMFResource(Paths.get(fileEntry.getAbsolutePath()));

						ArrayList<Node> designnodes;
						ArrayList<Edge> designedges;
						if (ModelUtility.getOCCIConfiguration(designtimemodel) != null) {
							Configuration configdesign = ModelUtility.getOCCIConfiguration(designtimemodel);
							designnodes = VisUtility.createNodes(configdesign.getResources());
							designedges = VisUtility.createEdges(configdesign.getResources());
						} else {
							designnodes = VisUtility
									.createNodes(ModelUtility.getResources(designtimemodel.getContents()));
							designedges = VisUtility
									.createEdges(ModelUtility.getResources(designtimemodel.getContents()));
						}

						StoreModel store = new StoreModel(Paths.get(fileEntry.getAbsolutePath()),
								FilenameUtils.getBaseName(fileEntry.getName()));

						store.setNodes(designnodes);
						store.setEdges(designedges);
						System.out.println("Model added: " + store);
						models.add(store);
					} catch (Exception e) {
						System.out.println("OCCI Configuration could not be loaded: " + fileEntry.getName());
					}
				}
			}
		}
		return models;
	}

	public void importGit(String repositoryURI) throws GitAPIException, IOException {

		String repoDir = createRepoDirName(repositoryURI);
		Path path = Paths.get(IMPORTDIR + repoDir);
		if (Files.notExists(path)) {
			File tarDir = new File(IMPORTDIR + repoDir);

			Git git = Git.cloneRepository().setURI(repositoryURI).setDirectory(tarDir).setNoCheckout(true).call();

			Repository repo = git.getRepository();

			Ref head = repo.findRef("refs/remotes/origin/master");
			String hash = head.getObjectId().getName();

			List<String> occicPaths = getOcciModelPaths(repo, head);
			git.checkout().setStartPoint(hash).addPaths(occicPaths).call();
			System.out.println("Repo " + repoDir + " successfully imported!");
		} else {
			System.out.println("Directory already imported: " + path.toString());
		}
	}

	private List<String> getOcciModelPaths(Repository repository, Ref head) {
		List<String> paths = new ArrayList<String>();
		try (RevWalk revWalk = new RevWalk(repository)) {
			RevCommit commit = revWalk.parseCommit(head.getObjectId());
			// and using commit's tree find the path
			RevTree tree = commit.getTree();
			System.out.println("Having tree: " + tree);

			// now try to find a specific file
			try (TreeWalk treeWalk = new TreeWalk(repository)) {
				treeWalk.addTree(tree);
				treeWalk.setRecursive(false);
				treeWalk.setFilter(PathSuffixFilter.create(".occic"));

				while (treeWalk.next()) {
					if (treeWalk.isSubtree()) {
						System.out.println("dir: " + treeWalk.getPathString());
						treeWalk.enterSubtree();
					} else {
						System.out.println(treeWalk.getPathString());
						paths.add(treeWalk.getPathString());
					}
				}
			}
			revWalk.dispose();
			return paths;
		} catch (IncorrectObjectTypeException e) {
			e.printStackTrace();
		} catch (CorruptObjectException e) {
			e.printStackTrace();
		} catch (MissingObjectException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return paths;
	}

	private String createRepoDirName(String repositoryURI) {
		return repositoryURI.substring(repositoryURI.lastIndexOf("/"), repositoryURI.lastIndexOf("."));
	}

	private Path getStoreModelPath(String repo, String modelName) throws FileNotFoundException {
		for (StoreModelRepository storeRepo : storeModelRepositories) {
			if (storeRepo.getRootDir().equals(repo)) {
				for (StoreModel storeModel : storeRepo.getStoreModels()) {
					if (storeModel.getTitle().equals(modelName)) {
						return storeModel.getPath();
					}
				}
			}
		}
		throw new FileNotFoundException("Could not find store model with title " + modelName);
	}
}