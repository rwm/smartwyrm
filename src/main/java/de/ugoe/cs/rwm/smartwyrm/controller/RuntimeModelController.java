/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.ugoe.cs.rwm.docci.connector.MartConnector;
import org.apache.commons.io.IOUtils;
import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.modmacao.occi.platform.Application;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.view.Edge;
import de.ugoe.cs.rwm.smartwyrm.view.Network;
import de.ugoe.cs.rwm.smartwyrm.view.Node;
import de.ugoe.cs.rwm.smartwyrm.view.VisUtility;

@Controller
public class RuntimeModelController {
	private static final Path LOCAL_RUNTIMEMODEL = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private static final String EXPORT_LOCATION = GlobalRegistry.getInstance().getStore() + "latestExport.occic".replace('\\', '/');

	private String address = GlobalRegistry.getInstance().getAddress();
	private String user = GlobalRegistry.getInstance().getUser();
	private int port = GlobalRegistry.getInstance().getPort();
	private String privateKey = GlobalRegistry.getInstance().getPrivateKey();
	private Connector conn;

	private RuntimeModelController() {
		if(address.equals("localhost")){
			this.conn = new LocalhostConnector("localhost", port, user);
		} else{
			this.conn = new MartConnector(address, port, user, privateKey);
		}
	}

	@GetMapping("/runtime")
	@ResponseBody
	public Network greeting(@RequestParam(name = "name", required = false, defaultValue = "Stranger") String name) {
		//Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		conn.loadRuntimeModel(LOCAL_RUNTIMEMODEL);
		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());

		Configuration config = (Configuration) occimodel.getContents().get(0);
		Network net = new Network();
		ArrayList<Node> nodes = VisUtility.createNodes(config.getResources());
		ArrayList<Edge> edges = VisUtility.createEdges(config.getResources());

		net.setNodes(nodes);
		net.setEdges(edges);
		return net;
	}

	@GetMapping("/download/runtime")
	@ResponseBody
	public String downloadRuntime() throws IOException {
		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Configuration config = (Configuration) occimodel.getContents().get(0);

		postProcessRuntimeModel(config);

		ResourceSet resourceSet = new ResourceSetImpl();
		URI uri = URI.createURI(EXPORT_LOCATION);
		org.eclipse.emf.ecore.resource.Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(config);
		resource.save(Collections.EMPTY_MAP);


		String str = "";
		try {
			str = IOUtils.toString(Paths.get(EXPORT_LOCATION).toUri(), "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@GetMapping("/download/runtime/exact")
	@ResponseBody
	public String downloadRuntimeExact() throws IOException {
		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Configuration config = (Configuration) occimodel.getContents().get(0);

		ResourceSet resourceSet = new ResourceSetImpl();
		URI uri = URI.createURI(EXPORT_LOCATION);
		org.eclipse.emf.ecore.resource.Resource resource = resourceSet.createResource(uri);
		resource.getContents().add(config);
		resource.save(Collections.EMPTY_MAP);


		String str = "";
		try {
			str = IOUtils.toString(Paths.get(EXPORT_LOCATION).toUri(), "utf-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	private void postProcessRuntimeModel(Configuration config) {

		removeFaultyAttributes(config);
		removeProviderSpecifics(config);
		resetResourceStates(config);
	}

	private void resetResourceStates(Configuration config){
		for(org.eclipse.cmf.occi.core.Resource res: config.getResources()){
			Kind kind = res.getKind();
			FSM fsm = getFSM(kind);
			if(fsm != null){
				String defaultStateLiteral = "";
				for(State state: fsm.getOwnedState()){
					if(state.isInitial()){
						defaultStateLiteral = state.getLiteral().getName();
					}
				}
				Attribute fsmAttr = fsm.getAttribute();
				for (AttributeState attr : res.getAttributes()) {
					if(isFSMAttr(fsmAttr, attr)){
						if(defaultStateLiteral.equals("") == false) {
							attr.setValue(defaultStateLiteral);
						}
					}
					if(attr.getName().contains("state") && attr.getName().contains("message")){
					    attr.setValue("");
                    }
				}
			}
		}
	}

	private FSM getFSM(Kind kind) {
		if(kind.getFsm() != null){
			return kind.getFsm();
		} else{
			if(kind.getParent() != null){
				return getFSM(kind.getParent());
			}
		}
		return null;
	}

	private boolean isFSMAttr(Attribute fsmAttr, AttributeState attr) {
		if(fsmAttr.getName().equals(attr.getName())){
			return true;
		} return false;
	}

	private void removeProviderSpecifics(Configuration config) {
		List<EObject> dlist = new BasicEList<EObject>();
		for(org.eclipse.cmf.occi.core.Resource res: config.getResources()){
			for(MixinBase mixb: res.getParts()){
				if(mixb.getMixin().getScheme().equals("http://schemas.modmacao.org/openstack/runtime#")){
					dlist.add(mixb);
				}
			}
			if(res.getId().equals(GlobalRegistry.getInstance().getManNWid())){
				dlist.add(res);
				for(Link rLink: res.getRlinks()){
					dlist.add(rLink);
				}
				for(Link link: res.getLinks()){
					dlist.add(link);
				}
			}
		}
		for (EObject obj : dlist) {
			EcoreUtil.delete(obj);
		}
	}


	private void removeFaultyAttributes(Configuration config) {
		List<EObject> dlist = new BasicEList<EObject>();
		for (org.eclipse.cmf.occi.core.Resource res : config.getResources()) {
			if(isApplication(res)){
				dlist.addAll(faultyApplicationAttributes(res));
			}
			dlist.addAll(getEmptyAttributes(res));
			for (Link link : res.getLinks()) {
				dlist.addAll(getEmptyAttributes(link));
			}
		}

		for (EObject obj : dlist) {
			EcoreUtil.delete(obj);
		}
	}

	private List<EObject> faultyApplicationAttributes(org.eclipse.cmf.occi.core.Resource res) {
		List<EObject> dlist = new BasicEList<EObject>();
		for(AttributeState attr: res.getAttributes()){
			if(checkAttrValue(attr, "occi.app.context", "0") || checkAttrValue(attr, "occi.app.url", "0")){
				dlist.add(attr);
			}
		}
		return dlist;
	}

	private boolean checkAttrValue(AttributeState attr, String name, String value){
		if(attr.getName().equals(name)){
			if(attr.getValue().equals(value)){
				return true;
			}
		}
		return false;
	}

	private boolean isApplication(org.eclipse.cmf.occi.core.Resource res) {
		if(res.getKind().getScheme().equals("http://schemas.modmacao.org/occi/platform#") && res.getKind().getTerm().equals("Application")){
			return true;
		} else if(res instanceof Application){
			return true;
		}
		return false;
	}

	private Collection<? extends EObject> getEmptyAttributes(Entity ent) {
		List<EObject> dlist = new BasicEList<EObject>();
		for (AttributeState attr : ent.getAttributes()) {
			if (attr.getValue() == null || attr.getValue().equals("")) {
				dlist.add(attr);
			}
		}
		for (MixinBase mixB : ent.getParts()) {
			for (AttributeState attr : mixB.getAttributes()) {
				if (attr.getValue() == null || attr.getValue().equals("")) {
					dlist.add(attr);
				}
			}
		}
		return dlist;
	}
}