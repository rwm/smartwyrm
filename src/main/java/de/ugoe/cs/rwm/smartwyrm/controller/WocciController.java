/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.MartDeployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.connector.MartConnector;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.view.Edge;
import de.ugoe.cs.rwm.smartwyrm.view.Node;
import de.ugoe.cs.rwm.smartwyrm.view.VisUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import de.ugoe.cs.rwm.tocci.occi2openstack.OCCI2OPENSTACKTransformator;
import de.ugoe.cs.rwm.wocci.enactor.DynamicEnactor;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.performer.Performer;
import de.ugoe.cs.rwm.wocci.performer.PerformerFactory;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.scheduler.SchedulerFactory;

@Controller
public class WocciController {
	private static final Path LOCAL_RUNTIMEMODEL = Paths.get(System.getProperty("user.home") + "/.rwm/runtime.occic");
	private static final Path LOCAL_REQRUNTIMEMODEL = Paths
			.get(System.getProperty("user.home") + "/.rwm/reqRuntime.occic");
	private Resource designtimemodel;
	private Path uploadedFile;

	private String manNWid = GlobalRegistry.getInstance().getManNWid();
	private String manNWRuntimeId = GlobalRegistry.getInstance().getManNWRuntimeId();
	private String sshKey = GlobalRegistry.getInstance().getSshKey();
	private String userData = GlobalRegistry.getInstance().getUserData();
	private int wocciCycle = GlobalRegistry.getInstance().getWocciCycle();
	private String archScheduler = GlobalRegistry.getInstance().getArchScheduler();
	private Performer performer;
	private String address = GlobalRegistry.getInstance().getAddress();
	private String user = GlobalRegistry.getInstance().getUser();
	private int port = GlobalRegistry.getInstance().getPort();
	private String privateKey = GlobalRegistry.getInstance().getPrivateKey();
	private Connector conn;

	private WocciController() {
		if (address.equals("localhost")) {
			this.conn = new LocalhostConnector("localhost", port, user);
		} else {
			this.conn = new MartConnector(address, port, user, privateKey);
		}
	}

	public Resource getDesigntimemodel() {
		return designtimemodel;
	}

	public void setDesigntimemodel(Resource designtimemodel) {
		this.designtimemodel = designtimemodel;
	}

	@GetMapping("/wocci")
	public String greeting(Model model, @RequestParam(value = "path", required = false) String path) {
		if (path != null) {
			this.uploadedFile = Paths.get(path);
			this.designtimemodel = ModelUtility.loadOCCIintoEMFResource(Paths.get(path));
		}
		// Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");
		ArrayList<Node> designnodes;
		ArrayList<Edge> designedges;
		if (designtimemodel != null) {
			if (ModelUtility.getOCCIConfiguration(designtimemodel) != null) {
				Configuration configdesign = ModelUtility.getOCCIConfiguration(designtimemodel);
				designnodes = VisUtility.createNodes(configdesign.getResources());
				designedges = VisUtility.createEdges(configdesign.getResources());
			} else {
				designnodes = VisUtility.createNodes(ModelUtility.getResources(designtimemodel.getContents()));
				designedges = VisUtility.createEdges(ModelUtility.getResources(designtimemodel.getContents()));
			}
			model.addAttribute("designnodes", designnodes);
			model.addAttribute("designedges", designedges);
		}

		conn.loadRuntimeModel(LOCAL_RUNTIMEMODEL);

		Resource occimodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Configuration config = (Configuration) occimodel.getContents().get(0);
		ArrayList<Node> nodes = VisUtility.createNodes(config.getResources());
		ArrayList<Edge> edges = VisUtility.createEdges(config.getResources());

		model.addAttribute("runnodes", nodes);
		model.addAttribute("runedges", edges);
		return "wocci";
	}

	@PostMapping("/wocci")
	public String upload(Model model, @RequestParam("file") MultipartFile file) throws IOException {

		byte[] bytes = file.getBytes();
		uploadedFile = Paths.get(GlobalRegistry.getInstance().getStore() + file.getOriginalFilename());
		Files.write(uploadedFile, bytes);

		Resource runmodel = ModelUtility.loadOCCIintoEMFResource(GlobalRegistry.getInstance().getRuntimePath());
		Configuration config = (Configuration) runmodel.getContents().get(0);
		ArrayList<Node> nodes = VisUtility.createNodes(config.getResources());
		ArrayList<Edge> edges = VisUtility.createEdges(config.getResources());

		model.addAttribute("runnodes", nodes);
		model.addAttribute("runedges", edges);

		ArrayList<Node> designnodes;
		ArrayList<Edge> designedges;

		Resource designmodel = ModelUtility.loadOCCIintoEMFResource(uploadedFile);
		if (ModelUtility.getOCCIConfiguration(designmodel) != null) {
			Configuration configdesign = ModelUtility.getOCCIConfiguration(designmodel);
			designnodes = VisUtility.createNodes(configdesign.getResources());
			designedges = VisUtility.createEdges(configdesign.getResources());
		} else {
			designnodes = VisUtility.createNodes(ModelUtility.getResources(designmodel.getContents()));
			designedges = VisUtility.createEdges(ModelUtility.getResources(designmodel.getContents()));
		}

		this.designtimemodel = designmodel;

		model.addAttribute("designnodes", designnodes);
		model.addAttribute("designedges", designedges);

		return "wocci";
	}

	@PostMapping("/wocci2openstack")
	public String transform() throws IOException {
		transform(this.designtimemodel);

		return "redirect:/wocci";
	}

	@PostMapping("/wocciEnact")
	public String enact(Model model) throws IOException {
		// Connector conn = new LocalhostConnector("localhost", 8080, "ubuntu");

		transform(this.designtimemodel);

		Deployer depl = new MartDeployer(conn, GlobalRegistry.getInstance().getSleep());
		// ArchitectureScheduler scheduler = new DiscreteScheduler(conn, depl);
		ArchitectureScheduler scheduler = SchedulerFactory.getArchScheduler(archScheduler, conn, depl);
		System.out.println("Archscheduler chosen: " + scheduler + " " + archScheduler);
		WorkflowEnactor enactor = new DynamicEnactor(conn);

		this.performer = PerformerFactory.getPerformer(conn, scheduler, enactor,
				GlobalRegistry.getInstance().getPerformer());

		performer.performWorkflow(this.designtimemodel);

		return "redirect:/wocci";
	}

	@PostMapping("/wocciStop")
	public String stop() throws IOException {
		if (this.performer != null) {
			this.performer.setFlag(false);
		}

		return "redirect:/wocci";
	}

	private void transform(Resource designtimemodel2) {
		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");
		try {
			trans.transform(designtimemodel, uploadedFile);
			designtimemodel = ModelUtility.loadOCCIintoEMFResource(uploadedFile);
			OCCI2OPENSTACKTransformator trans2 = new OCCI2OPENSTACKTransformator();

			trans2.setTransformationProperties(manNWRuntimeId, sshKey, userData, manNWid,
					GlobalRegistry.getInstance().getFlavor(), GlobalRegistry.getInstance().getImage(),
					GlobalRegistry.getInstance().getRemoteUser());

			try {
				trans2.transform(designtimemodel, uploadedFile);
			} catch (EolRuntimeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			designtimemodel = ModelUtility.loadOCCIintoEMFResource(uploadedFile);
		} catch (EolRuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}