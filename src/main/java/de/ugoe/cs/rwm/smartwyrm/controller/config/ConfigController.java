/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller.config;

import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;

@Controller
public class ConfigController {
	@GetMapping("/config")
	public String getConfig(Model model) {
		model.addAttribute("extensions", OcciRegistry.getInstance().getRegisteredExtensions());
		model.addAttribute("user", GlobalRegistry.getInstance().getUser());
		model.addAttribute("address", GlobalRegistry.getInstance().getAddress());
		model.addAttribute("port", GlobalRegistry.getInstance().getPort());
		model.addAttribute("sleep", GlobalRegistry.getInstance().getSleep());
		model.addAttribute("ansibleRoles", GlobalRegistry.getInstance().getAnsibleRoles());
		model.addAttribute("manNWid", GlobalRegistry.getInstance().getManNWid());
		model.addAttribute("manNWRuntimeId", GlobalRegistry.getInstance().getManNWRuntimeId());
		model.addAttribute("sshKey", GlobalRegistry.getInstance().getSshKey());
		model.addAttribute("userData", GlobalRegistry.getInstance().getUserData());
		model.addAttribute("image", GlobalRegistry.getInstance().getImage());
		model.addAttribute("flavor", GlobalRegistry.getInstance().getFlavor());
		model.addAttribute("remoteUser", GlobalRegistry.getInstance().getRemoteUser());
		model.addAttribute("pluginsFolder", GlobalRegistry.getInstance().getPluginsFolder());
		return "config";
	}

	@PostMapping("/config")
	public String postConfig(@RequestParam("user") String user, @RequestParam("address") String address,
			@RequestParam("port") int port, @RequestParam("sleep") int sleep, @RequestParam("manNWid") String manNWid,
			@RequestParam("manNWRuntimeId") String manNWRuntimeId, @RequestParam("sshKey") String sshKey,
			@RequestParam("userData") String userData, @RequestParam("ansibleRoles") String ansibleRoles,
			@RequestParam("flavor") String flavor, @RequestParam("image") String image,
			@RequestParam("remoteUser") String remoteUser, @RequestParam("pluginsFolder") String pluginsFolder) {
		System.out.println("Saving Configuration");
		GlobalRegistry.getInstance().setUser(user);
		GlobalRegistry.getInstance().setAddress(address);
		GlobalRegistry.getInstance().setPort(port);
		GlobalRegistry.getInstance().setSleep(sleep);
		GlobalRegistry.getInstance().setManNWRuntimeId(manNWRuntimeId);
		GlobalRegistry.getInstance().setManNWid(manNWid);
		GlobalRegistry.getInstance().setSshKey(sshKey);
		GlobalRegistry.getInstance().setUserData(userData);
		GlobalRegistry.getInstance().setAnsibleRoles(ansibleRoles);
		GlobalRegistry.getInstance().setFlavor(flavor);
		GlobalRegistry.getInstance().setImage(image);
		GlobalRegistry.getInstance().setRemoteUser(remoteUser);
		GlobalRegistry.getInstance().setPluginsFolder(pluginsFolder);
		return "redirect:/config";
	}
}