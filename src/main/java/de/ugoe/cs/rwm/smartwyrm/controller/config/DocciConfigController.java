/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller.config;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;

@Controller
public class DocciConfigController {
	@GetMapping("/config/docci")
	public String getConfig(Model model) {
		model.addAttribute("user", GlobalRegistry.getInstance().getUser());
		model.addAttribute("address", GlobalRegistry.getInstance().getAddress());
		model.addAttribute("port", GlobalRegistry.getInstance().getPort());
		model.addAttribute("privateKey", GlobalRegistry.getInstance().getPrivateKey());
		model.addAttribute("sleep", GlobalRegistry.getInstance().getSleep());
		return "config/docci";
	}

	@PostMapping("/config/docci")
	public String postConfig(@RequestParam("user") String user, @RequestParam("address") String address,
			@RequestParam("port") int port, @RequestParam("sleep") int sleep, @RequestParam("privateKey") String privateKey) {
		System.out.println("Saving Configuration");
		GlobalRegistry.getInstance().setUser(user);
		GlobalRegistry.getInstance().setAddress(address);
		GlobalRegistry.getInstance().setPort(port);
		GlobalRegistry.getInstance().setSleep(sleep);
		GlobalRegistry.getInstance().setPrivateKey(privateKey);
		return "redirect:/config/docci";
	}
}