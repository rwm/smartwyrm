/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller.config;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;

@Controller
public class TocciConfigController {
	@GetMapping("/config/tocci")
	public String getConfig(Model model) {
		model.addAttribute("manNWid", GlobalRegistry.getInstance().getManNWid());
		model.addAttribute("manNWRuntimeId", GlobalRegistry.getInstance().getManNWRuntimeId());
		model.addAttribute("sshKey", GlobalRegistry.getInstance().getSshKey());
		model.addAttribute("userData", GlobalRegistry.getInstance().getUserData());
		model.addAttribute("image", GlobalRegistry.getInstance().getImage());
		model.addAttribute("flavor", GlobalRegistry.getInstance().getFlavor());
		model.addAttribute("remoteUser", GlobalRegistry.getInstance().getRemoteUser());
		return "config/tocci";
	}

	@PostMapping("/config/tocci")
	public String postConfig(@RequestParam("manNWid") String manNWid,
			@RequestParam("manNWRuntimeId") String manNWRuntimeId, @RequestParam("sshKey") String sshKey,
			@RequestParam("userData") String userData, @RequestParam("flavor") String flavor,
			@RequestParam("image") String image, @RequestParam("remoteUser") String remoteUser) {
		System.out.println("Saving Configuration");
		GlobalRegistry.getInstance().setManNWRuntimeId(manNWRuntimeId);
		GlobalRegistry.getInstance().setManNWid(manNWid);
		GlobalRegistry.getInstance().setSshKey(sshKey);
		GlobalRegistry.getInstance().setUserData(userData);
		GlobalRegistry.getInstance().setFlavor(flavor);
		GlobalRegistry.getInstance().setImage(image);
		GlobalRegistry.getInstance().setRemoteUser(remoteUser);
		return "redirect:/config/tocci";
	}
}