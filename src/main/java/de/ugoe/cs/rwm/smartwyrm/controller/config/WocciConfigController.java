/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller.config;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;

@Controller
public class WocciConfigController {
	@GetMapping("/config/wocci")
	public String getConfigWocci(Model model) {
		model.addAttribute("wocciCycle", GlobalRegistry.getInstance().getWocciCycle());
		model.addAttribute("archScheduler", GlobalRegistry.getInstance().getArchScheduler());
		model.addAttribute("performer", GlobalRegistry.getInstance().getPerformer());
		System.out.println("Performer " + GlobalRegistry.getInstance().getPerformer());
		return "config/wocci";
	}

	@PostMapping("/config/wocci")
	public String postConfig(@RequestParam("wocciCycle") int wocciCycle,
			@RequestParam("archScheduler") String archScheduler, @RequestParam("performer") String performer) {
		System.out.println("Saving Configuration");
		GlobalRegistry.getInstance().setWocciCycle(wocciCycle);
		GlobalRegistry.getInstance().setArchScheduler(archScheduler);
		GlobalRegistry.getInstance().setPerformer(performer);
		System.out.println("Performer: " + performer);
		return "redirect:/config/wocci";
	}
}