package de.ugoe.cs.rwm.smartwyrm.controller.entitybuilder;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.impl.StringTypeImpl;
import org.eclipse.cmf.occi.core.util.OcciHelper;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

    public class AttributeBuilder {
        private String name;
        private String def;
        private String description;
        private boolean required;
        private String pattern;
       // public DataType type;

        private final String urlpattern= "^(https?|ftp|file):\\/\\/[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        public String getName() {
            return name;
        }
        public String getDefault() {
            return def;
        }
        public boolean getRequired() {
            return required;
        }
        public String getDescription() {
            return description;
        }

        public String getPattern() {
            return pattern;
        }


        public AttributeBuilder(Attribute attr){
            this.name = attr.getName();
            this.def = attr.getDefault();
            this.description = attr.getDescription();
            this.required = attr.isRequired();

            DataType dt = attr.getType();
            if(dt != null && dt.eIsProxy() == false){
                if(attr.getType() instanceof StringTypeImpl) {
                    StringTypeImpl impl = (StringTypeImpl) attr.getType();
                    this.pattern = impl.getPattern();
                }
                //else if(attr.getType(). instanceof java.net.URL){
                //    System.out.println(dt);
                else if(dt.getName().equals("URL") || dt.getName().equals("URI")) {
                   this.pattern = urlpattern;
                }
            }
        }
    }




