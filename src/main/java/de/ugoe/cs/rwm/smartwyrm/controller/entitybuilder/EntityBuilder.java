package de.ugoe.cs.rwm.smartwyrm.controller.entitybuilder;

import de.ugoe.cs.rwm.docci.ModelUtility;
import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.impl.StringTypeImpl;
import org.eclipse.cmf.occi.core.util.OcciHelper;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class EntityBuilder {
    private String extension;
    private EList<KindBuilder> linkKinds;
    private EList<KindBuilder> resourceKinds;


    public String getExtension() {
        return extension;
    }

    public EList<KindBuilder> getLinkKinds() {
        return linkKinds;
    }

    public EList<KindBuilder> getResourceKinds() {
        return resourceKinds;
    }


    public EntityBuilder(String extScheme){
        Extension extension = OcciHelper.loadExtension(extScheme);
        this.extension = extScheme;
        this.resourceKinds = new BasicEList<KindBuilder>();
        this.linkKinds = new BasicEList<KindBuilder>();
        for(Kind kind: extension.getKinds()){
            if(kindInherits(kind, "Resource")) {
                this.resourceKinds.add(new KindBuilder(kind));
            } else {
                this.linkKinds.add(new KindBuilder(kind));
            }
        }
    }

    private boolean kindInherits(Kind kind, String parent) {
        if(kind.getParent() == null || kind.getParent().getName().equals("Entity")){
            return false;
        }
        else if(kind.getParent().getName().equals(parent)){
            return true;
        } else{
            return kindInherits(kind.getParent(), parent);
        }
    }
}


