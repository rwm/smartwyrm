/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.controller.entitybuilder;

import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.connector.LocalhostConnector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.executor.ExecutorFactory;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.util.OcciHelper;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.RemoteEndpoint;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public class EntityBuilderController {
	private Collection<EntityBuilder> entBuilder;
	private OCCIFactory fac = OCCIFactory.eINSTANCE;

	@RequestMapping(value="/builder/entity", produces= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Collection<EntityBuilder> getEntityBuilder() {
		if(entBuilder == null) {
			System.out.println("Generating Extension Builder");
			Collection<EntityBuilder> extensions = new ArrayList<EntityBuilder>();

			for (String str : OcciRegistry.getInstance().getRegisteredExtensions()) {
				if(hasAtLeastOneKind(OcciHelper.loadExtension(str))){
					EntityBuilder build = new EntityBuilder(str);
					extensions.add(build);
				}
			}
			entBuilder = extensions;
			return extensions;
		}
		else{
			System.out.println("Using Cached Extension Builder");
			return entBuilder;
		}
	}

	private boolean hasAtLeastOneKind(Extension loadExtension) {
		return loadExtension.getKinds().size() >= 1;
	}

	@PostMapping("/builder/resource")
	public String buildResource(@RequestParam("entityBuild") String entityString) throws ParseException {
		buildEntity(entityString, Resource.class);
		return "wocci";
	}


	@PostMapping("/builder/link")
	public String buildLink(@RequestParam("entityBuild") String entityString) throws ParseException {
		buildEntity(entityString, Link.class);
		return "wocci";
	}


	private void buildEntity(String entityString, Class clazz) throws ParseException{
		org.json.simple.JSONObject jobj = (org.json.simple.JSONObject) new JSONParser().parse(entityString);

		Extension ext = OcciHelper.loadExtension((String) jobj.get("ext"));
		Entity entity;
		if(clazz.equals(Link.class)) {
			entity = (Link) fac.createLink();
		} else{
			entity = (Resource) fac.createResource();
		}

		entity.setKind(OcciHelper.getKindByTerm(ext, (String) jobj.get("kind")));

		entity.getParts().addAll(gatherMixins(jobj));



		entity.getAttributes().addAll(gatherAttributes(jobj));

		Connector conn = new LocalhostConnector(GlobalRegistry.getInstance().getAddress(), GlobalRegistry.getInstance().getPort(), GlobalRegistry.getInstance().getUser());
		Executor exec = ExecutorFactory.getExecutor("Mart", conn);
		exec.executeOperation("PUT", entity, null);

	}

	private Collection<? extends MixinBase> gatherMixins(JSONObject jobj) {
		List<MixinBase> mixinBases = new BasicEList<MixinBase>();
		JSONArray mixinsArr = (JSONArray) jobj.get("mixins");
		for(Object obj: mixinsArr){
			String mixSchemeTerm = (String) obj;
			String mixScheme = mixSchemeTerm.split("#")[0];
			String mixTerm = mixSchemeTerm.split("#")[1];
			Mixin mix = null;
			try {
				mix = OcciHelper.getMixinByTerm(OcciHelper.loadExtension(mixScheme), mixTerm);
			}catch(Exception e){
				System.out.println("Mixin Schem does not have the exact same scheme as its Extension!");

				for(String registeredExtension: OcciRegistry.getInstance().getRegisteredExtensions()){
					String extStart = registeredExtension.split("#")[0];
					if(mixScheme.startsWith(extStart)){
						try {
							mix = OcciHelper.getMixinByTerm(OcciHelper.loadExtension(registeredExtension), mixTerm);
							break;
						} catch(Exception e2){
							System.out.println("Difficulties loading Mixin from Extension based on its scheme!");
						}
					}
				}
			}
			MixinBase part = fac.createMixinBase();
			part.setMixin(mix);
			//entity.getParts().add(part);
			mixinBases.add(part);
		}
		return mixinBases;
	}

	private Collection<? extends AttributeState> gatherAttributes(JSONObject jobj) {
		List<AttributeState> attributes= new BasicEList<AttributeState>();
		JSONArray attrArr = (JSONArray) jobj.get("attributes");
		for(Object obj: attrArr){
			JSONObject jAttr = (JSONObject) obj;
			if(jAttr.get("value").equals("") == false) {
				AttributeState attrState = fac.createAttributeState();
				attrState.setName((String) jAttr.get("name"));
				attrState.setValue((String) jAttr.get("value"));
				attributes.add(attrState);
			}
		}
		return attributes;
	}
}