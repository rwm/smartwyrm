package de.ugoe.cs.rwm.smartwyrm.controller.entitybuilder;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.util.OcciHelper;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import java.util.HashMap;
import java.util.HashSet;

public class KindBuilder {
    private String term;
    private String scheme;
    private EList<AttributeBuilder> attributes;
    private HashMap<String, HashSet<MixinBuilder>> applicableMixins;

    public String getTerm() {
        return term;
    }


    public String getScheme() {
        return scheme;
    }


    public EList<AttributeBuilder> getAttributes() {
        return attributes;
    }

    public HashMap<String, HashSet<MixinBuilder>> getApplicableMixins() {
        return applicableMixins;
    }


    public KindBuilder(Kind kind){
        this.attributes = new BasicEList<>();
        term = kind.getTerm();
        scheme = kind.getScheme();
        this.applicableMixins = generateApplicableMixins();
        generateAttributes(kind);

    }

    private void generateAttributes(Kind kind) {
        for(Attribute attr: kind.getAttributes()){
            if(attr.getName().equals("occi.core.id") == false && isState(kind, attr) ==false) {
                this.attributes.add(new AttributeBuilder(attr));
            }
        }
        addParentAttributes(kind.getParent());
    }

    private void addParentAttributes(Kind parent) {
        if(parent != null) {
            for (Attribute attr : parent.getAttributes()) {
                if(attr.getName().equals("occi.core.id") == false && isState(parent, attr) ==false) {
                    this.attributes.add(new AttributeBuilder(attr));
                }
            }
            if (parent.getParent() != null) {
                addParentAttributes(parent.getParent());
            }
        }
    }

    private boolean isState(Kind kind, Attribute attr) {
        if(kind.getFsm() != null){
            if(kind.getFsm().getAttribute() != null){
                if(kind.getFsm().getAttribute().getName() != null){
                    if(kind.getFsm().getAttribute().getName().equals(attr.getName())){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private HashMap<String, HashSet<MixinBuilder>> generateApplicableMixins(){
        HashMap<String, HashSet<MixinBuilder>> applicableMixins = new HashMap<>();
        for(String str: OcciRegistry.getInstance().getRegisteredExtensions()){
            Extension ext = OcciHelper.loadExtension(str);
            for(Mixin mix: ext.getMixins()){
                if(canBeApplied(mix)){
                    if(applicableMixins.containsKey(mix.getScheme()) == false){
                        applicableMixins.put(mix.getScheme(), new HashSet<MixinBuilder>());

                    }
                    applicableMixins.get(mix.getScheme()).add(new MixinBuilder(mix));
                }
                if(parentCanBeApplied(mix)){
                    if(applicableMixins.containsKey(mix.getScheme()) == false){
                        applicableMixins.put(mix.getScheme(), new HashSet<MixinBuilder>());

                    }
                    applicableMixins.get(mix.getScheme()).add(new MixinBuilder(mix));
                }
            }
        }
        return applicableMixins;
    }

    private boolean canBeApplied(Mixin mix){
        for(Kind kind: mix.getApplies()) {
            if (kind.getName().toLowerCase().equals(this.term.toLowerCase()) && kind.getScheme().equals(this.scheme)) {
                return true;
            }
        }
        return false;
    }


    private boolean parentCanBeApplied(Mixin mix){
        for(Mixin parent: mix.getDepends()){
            if(canBeApplied(parent)){
                return true;
            }
            if(parentCanBeApplied(parent)){
                return true;
            }
        }
        return false;
    }
}