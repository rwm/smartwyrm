package de.ugoe.cs.rwm.smartwyrm.controller.entitybuilder;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.core.impl.StringTypeImpl;
import org.eclipse.cmf.occi.core.util.OcciHelper;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

    public class MixinBuilder{
        private String term;
        private EList<AttributeBuilder> attributes;
        private String scheme;

        public String getTerm() {
            return term;
        }

        public EList<AttributeBuilder> getAttributes() {
            return attributes;
        }

        public void setAttributes(EList<AttributeBuilder> attributes) {
            this.attributes = attributes;
        }

        public String getScheme() {
            return scheme;
        }

        MixinBuilder(Mixin mix){
            this.attributes = new BasicEList<>();
            term = mix.getTerm();
            scheme = mix.getScheme();
            generateAttributes(mix);
        }

        private void generateAttributes(Mixin mix) {
            for(Attribute attr: mix.getAttributes()){
                    this.attributes.add(new AttributeBuilder(attr));
            }
            for(Mixin parent: mix.getDepends()){
                addParentAttributes(parent);
            }
        }

        private void addParentAttributes(Mixin parent) {
            if(parent != null) {
                for (Attribute attr : parent.getAttributes()) {
                        this.attributes.add(new AttributeBuilder(attr));
                }
                if (parent.getDepends() != null) {
                    for(Mixin pparent: parent.getDepends()) {
                        addParentAttributes(pparent);
                    }
                }
            }
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (!(obj instanceof MixinBuilder))
                return false;
            if (obj == this)
                return true;
            return (this.getTerm().equals(((MixinBuilder) obj).getTerm()) && this.getScheme().equals(((MixinBuilder) obj).getScheme()));
        }

        @Override
        public int hashCode() {
            String id = this.getScheme() + this.getScheme();
            return id.hashCode();
        }
    }



