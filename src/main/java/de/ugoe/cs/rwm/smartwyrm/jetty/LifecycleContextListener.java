/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.jetty;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.cmf.occi.mart.MART;

import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;

public class LifecycleContextListener {

	private static String pluginsFolder = GlobalRegistry.getInstance().getPluginsFolder();

	public static void addPluginsToClassPath() throws Exception {
		// need to do add path to Classpath with reflection since the
		// URLClassLoader.addURL(URL url) method is protected:
		// Detect in plugins directory...
		try {
			if (pluginsFolder == null) {
				System.out.println("plugins directory is not set.");
				return;
			}
			System.out.println("Registering Plugins contained in: " + pluginsFolder);
			List<String> files = fileList(pluginsFolder);
			for (String filename : files) {
				System.out.println("Plugin jar detected: " + filename);
				if (filename.contains("connector") == false) {
					File f = new File(filename);
					URI u = f.toURI();
					System.out.println(u.toURL());

					// URLClassLoader urlClassLoader = (URLClassLoader)
					// ClassLoader.getSystemClassLoader();
					URLClassLoader urlClassLoader = (URLClassLoader) Thread.currentThread().getContextClassLoader();
					Class<URLClassLoader> urlClass = URLClassLoader.class;

					Method method = urlClass.getDeclaredMethod("addURL", new Class[] { URL.class });
					method.setAccessible(true);
					method.invoke(urlClassLoader, new Object[] { u.toURL() });
				}
			}
		} catch (NoSuchFileException e) {
			System.out.println(
					"Plugins folder could not be found! Skipping extension registration. Please provide the correct path to the martserver-plugins folder within the settings of SmartWYRM.");
		}
	}

	/**
	 * List files on a local directory (use java nio).
	 *
	 * @param directory
	 * @return List of files in directory
	 * @throws Exception
	 */
	public static List<String> fileList(String directory) throws Exception {
		List<String> fileNames = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
			for (Path path : directoryStream) {
				fileNames.add(path.toString());
			}
		} catch (IOException ex) {
			System.out.println("Exception thrown : " + ex.getClass().getName() + " , message: " + ex.getMessage());
			throw ex;
		}
		return fileNames;
	}

	/**
	 * Get a resource path from the classpath.
	 *
	 * @param path
	 *            the path into the classpath.
	 * @return the resource path.
	 */
	public static String getResourceFromClasspath(String path) {
		java.net.URL resourceUrl = MART.class.getResource(path);
		return (resourceUrl != null) ? resourceUrl.toExternalForm() : null;
	}

	/**
	 * Report various JVM information.
	 */
	public static void reportJavaInformation() {
		// Getting the runtime reference from system.
		java.lang.Runtime runtime = java.lang.Runtime.getRuntime();
		System.out.println("Java Runtime available processor = " + runtime.availableProcessors());
		System.out.println("Java Runtime max memory   = " + runtime.maxMemory());
		System.out.println("Java Runtime total memory = " + runtime.totalMemory());
		System.out.println("Java Runtime free memory  = " + runtime.freeMemory());

		int mb = 1024 * 1024;
		System.out.println("##### Heap utilization statistics [MB] #####");
		// Print used memory
		System.out.println("Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);
		// Print free memory
		System.out.println("Free Memory:" + runtime.freeMemory() / mb);
		// Print total available memory
		System.out.println("Total Memory:" + runtime.totalMemory() / mb);
		// Print Maximum available memory
		System.out.println("Max Memory:" + runtime.maxMemory() / mb);
	}

	/**
	 * Load a class.
	 */
	private static Class<?> loadClass(final String className) {
		try {
			System.out.println("Loading class " + className + "...");
			return MART.class.getClassLoader().loadClass(className);
		} catch (ClassNotFoundException e) {
			System.out.println("Class " + className + " not found!");
		}
		return null;
	}

	/**
	 * Instantiate a class.
	 */
	private static Object newInstance(final String className) {
		Class<?> clazz = loadClass(className);
		if (clazz != null) {
			try {
				System.out.println("Instantiating class " + className + "...");
				return clazz.newInstance();
			} catch (InstantiationException e) {
				System.out.println("Instantiation exception!");
			} catch (IllegalAccessException e) {
				System.out.println("Illegal access exception!");
			}
		}
		return null;
	}

}
