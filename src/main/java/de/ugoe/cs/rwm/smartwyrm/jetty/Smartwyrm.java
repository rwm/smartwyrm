/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.jetty;

import java.io.File;

import javax.annotation.PreDestroy;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.event.EventListener;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.docci.Deployer;
import de.ugoe.cs.rwm.docci.ModelUtility;
import de.ugoe.cs.rwm.docci.appdeployer.MartAppDeployerMaster;
import de.ugoe.cs.rwm.docci.connector.Connector;
import de.ugoe.cs.rwm.docci.executor.Executor;
import de.ugoe.cs.rwm.docci.provisioner.Provisioner;
import de.ugoe.cs.rwm.docci.retriever.ModelRetriever;
import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.SettingsHelper;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.wocci.enactor.WorkflowEnactor;
import de.ugoe.cs.rwm.wocci.enactor.executor.TaskExecutor;
import de.ugoe.cs.rwm.wocci.performer.SequentialPerformer;
import de.ugoe.cs.rwm.wocci.scheduler.ArchitectureScheduler;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;
import pcg.PcgPackage;
import pog.PogPackage;

@ServletComponentScan
@SpringBootApplication(scanBasePackages = "de.ugoe.cs.rwm.smartwyrm.controller")
public class Smartwyrm {

	public static void main(String[] args) throws Exception {
		System.out.println("POG");
		PogPackage.eINSTANCE.eClass();
		System.out.println("Y");

		System.out.println("PCG");
		PcgPackage.eINSTANCE.eClass();
		System.out.println(PcgPackage.eINSTANCE.eClass());
		System.out.println("Y");

		// If the OCCI package can not be loaded it is caused by duplicate namings in
		// the shadowjar
		System.out.println("OCCI");
		OCCIPackage.eINSTANCE.eClass();
		System.out.println("Y");

		System.out.println(ClassLoader.getSystemClassLoader());

		Logger.getLogger(ModelUtility.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Executor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(Transformator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Connector.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(ModelRetriever.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Provisioner.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(Deployer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(WorkflowEnactor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(WorkflowUtility.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(MartAppDeployerMaster.class.getName()).setLevel(Level.OFF);
		Logger.getLogger(TaskExecutor.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(SequentialPerformer.class.getName()).setLevel(Level.INFO);
		Logger.getLogger(ArchitectureScheduler.class.getName()).setLevel(Level.INFO);

		System.getProperties().put("server.port", 8081);
		SpringApplication.run(Smartwyrm.class, args);

	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {
		new SettingsHelper().getProperties();

		File storeDir = new File(GlobalRegistry.getInstance().getStore());
		if (!storeDir.exists()) {
			System.out.println("Creating Store Directory.");
			storeDir.mkdirs();
		}
		try {
			LifecycleContextListener.addPluginsToClassPath();

		} catch (Exception ex) {
			System.out.println("Plugins not installed : " + ex.getClass().getName() + " --> " + ex.getMessage());
			ex.printStackTrace();
		}

		// Removes duplicate registries!
		org.eclipse.cmf.occi.mart.MART.initMART();

		System.out.println();
		System.out.println(OcciRegistry.getInstance().getRegisteredExtensions());
	}

	@PreDestroy
	public void destroy() {
		System.out.println("Storing Configurations.");
		new SettingsHelper().storeProperties();
		System.out.println("Configuration Stored");
	}
}
