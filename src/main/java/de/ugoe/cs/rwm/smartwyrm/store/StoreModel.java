package de.ugoe.cs.rwm.smartwyrm.store;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.List;

import de.ugoe.cs.rwm.smartwyrm.GlobalRegistry;
import de.ugoe.cs.rwm.smartwyrm.view.Edge;
import de.ugoe.cs.rwm.smartwyrm.view.Node;

public class StoreModel {
	private String title;
	private Path path;
	private List<Edge> edges;
	private List<Node> nodes;

	public StoreModel(Path path, String title) {
		this.path = path;
		this.title = title;
	}

	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}


}
