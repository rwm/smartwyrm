package de.ugoe.cs.rwm.smartwyrm.store;

import java.util.ArrayList;
import java.util.List;

public class StoreModelRepository {
    private List<StoreModel> storeModels;
    private String rootDir;

    public StoreModelRepository(String rootDir) {
        this.rootDir = rootDir.replace(".","");
        this.storeModels = new ArrayList<StoreModel>();
    }

    public List<StoreModel> getStoreModels() {
        return storeModels;
    }

    public void setStoreModels(List<StoreModel> storeModels) {
        this.storeModels = storeModels;
    }

    public String getRootDir() {
        return rootDir;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
