/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.view;

import java.util.List;

import org.eclipse.cmf.occi.core.AttributeState;

public class Edge {
	private String from;
	private String to;
	private String label;
	private String id;
	public List<AttributeState> attributes;
	public List<String> parts;
	public String kind;
	public List<String> actions;
	public EdgeColor color;
	public Boolean dashes = false;
	private Boolean smooth = false;

	public EdgeColor getColor() {
		return color;
	}

	public void setColor(EdgeColor color) {
		this.color = color;
	}

	public Boolean getDashes() {
		return dashes;
	}

	public void setDashes(Boolean dashes) {
		this.dashes = dashes;
	}

	public List<AttributeState> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<AttributeState> attributes) {
		this.attributes = attributes;
	}

	public List<String> getParts() {
		return parts;
	}

	public void setParts(List<String> parts) {
		this.parts = parts;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public List<String> getActions() {
		return actions;
	}

	public void setActions(List<String> actions) {
		this.actions = actions;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String arrows = "to";

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String src) {
		this.from = src;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String tar) {
		this.to = tar;
	}

	public String getArrows() {
		return arrows;
	}

	public void setArrows(String arrows) {
		this.arrows = arrows;
	}

    public void setSmooth(boolean b) {
		this.smooth = b;
    }
}
