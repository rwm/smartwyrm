/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.view;

import java.util.List;

import org.eclipse.cmf.occi.core.AttributeState;

public class Node {
	public String id;
	public String label;
	public String group;
	public String color;
	public String bordercolor;
	public List<AttributeState> attributes;
	public List<String> parts;
	public String kind;
	public List<String> actions;
	public int level;
	public int x;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public List<AttributeState> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<AttributeState> attributes) {
		this.attributes = attributes;
	}

	public List<String> getParts() {
		return parts;
	}

	public void setParts(List<String> parts) {
		this.parts = parts;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public List<String> getActions() {
		return actions;
	}

	public void setActions(List<String> actions) {
		this.actions = actions;
	}

	public String getColor() {
		return color;
	}

	public String getBordercolor() {
		return bordercolor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setBordercolor(String bordercolor) {
		this.bordercolor = bordercolor;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}

		Node node = (Node) obj;
		return (node.id.equals(this.id));
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}

}
