/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.smartwyrm.view;

import java.util.*;

import org.eclipse.cmf.occi.core.*;
import org.eclipse.cmf.occi.docker.Container;
import org.eclipse.cmf.occi.infrastructure.Compute;
import org.eclipse.cmf.occi.infrastructure.Network;
import org.eclipse.cmf.occi.infrastructure.Networkinterface;
import org.eclipse.cmf.occi.infrastructure.Storage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.pivot.values.InvalidValueException;
import org.modmacao.occi.platform.Application;
import org.modmacao.occi.platform.Component;
import org.modmacao.occi.platform.Componentlink;

import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.wocci.utility.WorkflowUtility;
import monitoring.Sensor;
import workflow.*;

public class VisUtility {
	private static int MINTASKLEVEL = 4;

	private static Node createNode(org.eclipse.cmf.occi.core.Resource res) {
		Node node = new Node();
		node.setId(res.getId());
		node.setLabel(res.getTitle());
		node.setAttributes(res.getAttributes());
		node.getAttributes().addAll(getPartAttributes(res));

		node.setGroup(getGroupOf(res));

		node.setActions(getActions(res));
		node.setParts(getParts(res));
		for (String str : node.getParts()) {
			if (str.contains("#Mixin for part could not be loaded")) {
				node.setLabel("Error!");
			}
		}

		try {
			node.setKind(res.getKind().getScheme() + res.getKind().getTerm());
		} catch (InvalidValueException e) {
			node.setLabel("Error!");
			node.setKind("Kind#Could not be loaded!");
		}

		if (node.getLabel() == null) {
			for (AttributeState attr : res.getAttributes()) {
				if (attr.getName().equals("occi.core.title")) {
					node.setLabel(attr.getValue());
				}
			}
		}

		return node;
	}

	public static ArrayList<Node> createNodes(EList<org.eclipse.cmf.occi.core.Resource> resources) {
		Set<Node> nodes = new HashSet<Node>();

		// List<Component> components = new ArrayList<Component>();
		// List<Application> apps = new ArrayList<Application>();
		List<Task> tasks = new ArrayList<Task>();

		int i = containsContainerNodes(resources);
		for (org.eclipse.cmf.occi.core.Resource res : EcoreUtil.copyAll(resources)) {

			Node node = null;
			if (res instanceof Storage) {
				node = createNode(res);
				node.setGroup(((Storage) res).getOcciStorageState().getName() + "_storage");
				node.setLevel(0);
			} else if (isNetwork(res)) {
				node = createNode(res);
				if (res instanceof Network) {
					node.setGroup(((Network) res).getOcciNetworkState().getName() + "_network");
				} else {
					node.setGroup("active_network");
				}
				node.setLevel(0);
			} else if (res instanceof Container) {
				node = createNode(res);
				node.setGroup(((Container) res).getOcciComputeState().getName() + "_container");
				node.setLevel(1 + i);
			} else if (res instanceof Compute) {
				node = createNode(res);
				node.setGroup(((Compute) res).getOcciComputeState().getName() + "_compute");
				node.setLevel(1);
			} else if (res instanceof Component) {
				node = createNode(res);
				// components.add((Component) res);
				node.setGroup(((Component) res).getOcciComponentState().getName() + "_component");
				if (isTaskExecutable(res)) {
					node.setLevel(3 + i);
				} else {
					node.setLevel(2 + i);
				}
			} else if (res instanceof Application) {
				// apps.add((Application) res);
				node = createNode(res);
				if (res instanceof Sensor) {
					node.setGroup(((Application) res).getOcciAppState().getName() + "_sensor");
					node.setLevel(3 + i);
				} else {
					node.setGroup(((Application) res).getOcciAppState().getName() + "_application");
					node.setLevel(3 + i);
				}
			} else if (res instanceof Task) {
				tasks.add((Task) res);
			} else {
				node = createNode(res);
				node.setGroup("default");
				node.setLevel(5 + i);
			}

			if (node != null) {
				if (node.getLabel() != null && node.getLabel().equals("Error!")) {
					node.setGroup("initialization_error");

				}
				nodes.add(node);
			}

		}

		/*
		 * Component max = getComponentsWithMaxCompLinks(components); int maxLinks =
		 * getComponentLinks(max).size(); int componentBaseLevel = 2; componentBaseLevel
		 * += maxLinks - 1; nodes.addAll(createComponentNodes(components));
		 *
		 * for(Application app: apps) { Node node = createNode(app);
		 * node.setGroup(app.getOcciAppState().getName() + "_application");
		 * node.setLevel(componentBaseLevel + 1); nodes.add(node); }
		 */
		ArrayList<Node> nodesAsList = new ArrayList<Node>();
		nodesAsList.addAll(nodes);

		if (tasks.isEmpty() == false) {
			nodesAsList.addAll(createTaskNodes((List<Task>) EcoreUtil.copyAll(tasks)));
			setXPosition(tasks, nodesAsList);
		}

		return nodesAsList;
	}

	private static int containsContainerNodes(EList<Resource> resources) {
		for (Resource res : resources) {
			if (res instanceof Container) {
				return 1;
			}
		}
		return 0;
	}

	private static boolean isNetwork(Resource res) {
		if (res instanceof Network || (res.getKind().getTerm().equals("network")
				&& res.getKind().getScheme().equals("http://schemas.ogf.org/occi/infrastructure#"))) {
			return true;
		}
		return false;
	}

	private static boolean isTaskExecutable(Resource res) {
		for (Link link : res.getRlinks()) {
			if (link instanceof Executionlink) {
				return true;
			}
		}
		return false;
	}

	private static int calculateMaxTaskLevel(List<Task> tasks) {
		return MINTASKLEVEL + getTaskLevelDistance(tasks);
	}

	private static int getTaskLevelDistance(List<Task> tasks) {
		if (tasks.isEmpty()) {
			return 0;
		}
		Task max = getTaskWithMaxDependencies(tasks);
		int maxLinks = getTaskdependencies(max).size();
		if (getNesteddependencies(max).isEmpty() == false) {
			List<Task> subgraph = new ArrayList<>();
			for (Nesteddependency nest : getNesteddependencies(max)) {
				if (nest.getTarget() instanceof Loop) {
					Loop loop = (Loop) nest.getTarget();
					subgraph.addAll(WorkflowUtility.getLoopedTasks(loop));
				}
			}
			maxLinks += getTaskLevelDistance(subgraph);
		}
		return maxLinks;
	}

	private static Collection<? extends Node> createTaskNodes(List<Task> tasks) {
		List<Node> nodes = new ArrayList<Node>();
		int maxTaskLevel = calculateMaxTaskLevel(tasks);

		while (tasks.isEmpty() == false) {
			Task max = getTaskWithMaxDependencies(tasks);
			Node maxNode = createNode(max);
			maxNode.setLevel(maxTaskLevel);
			// System.out.println("Max: (" +maxNode.getLevel() +") " + max);
			nodes.add(maxNode);

			nodes.addAll(createSubsequentNodes(max, maxNode, tasks, maxTaskLevel));

			tasks.remove(max);
		}
		// setXPosition(tasks, nodes);
		return nodes;
	}

	private static List<Node> createSubsequentNodes(Task max, Node maxNode, List<Task> tasks, int maxTaskLevel) {
		List<Node> nodes = new ArrayList<Node>();

		nodes.addAll(createNestedNodes(max, maxNode, tasks));
		nodes.addAll(createSequencedNodes(max, maxNode, tasks));

		return nodes;
	}

	private static Collection<? extends Node> createSequencedNodes(Task max, Node maxNode, List<Task> tasks) {
		List<Node> nodes = new ArrayList<Node>();
		List<Task> toRemove = new ArrayList<Task>();
		for (int i = 0; i < getTaskdependencies(max).size(); i++) {
			Taskdependency res = getTaskdependencies(max).get(i);
			if (res.getTarget() instanceof Task) {
				Task tarTask = (Task) res.getTarget();
				if (tasks.contains(tarTask)) {
					Node node = createNode(tarTask);
					node.setLevel(maxNode.getLevel());
					if (max instanceof Loop) {
						if (WorkflowUtility.buildsLoopForward(tarTask, max)) {
							node.setLevel(maxNode.getLevel() - 1);
						}
					} else if (max instanceof Decision) {
						if (maxNode.getLevel() - i >= MINTASKLEVEL) {
							node.setLevel(maxNode.getLevel() - i);
						}
					}
					// System.out.println("Follow: (" +node.getLevel() +") " + tarTask);
					nodes.add(node);
					toRemove.add(tarTask);
				}
			}
		}
		tasks.removeAll(toRemove);

		return nodes;
	}

	private static List<Node> createNestedNodes(Task max, Node maxNode, List<Task> tasks) {
		List<Node> nodes = new ArrayList<Node>();
		for (int i = 0; i < getNesteddependencies(max).size(); i++) {
			Nesteddependency res = getNesteddependencies(max).get(i);
			if (tasks.contains(res.getTarget())) {
				Node node = createNode(res.getTarget());
				node.setLevel(maxNode.getLevel() - 1);
				tasks.remove(res.getTarget());
				nodes.add(node);
				// System.out.println("Nested: (" +node.getLevel() +") " + res.getTarget());
				nodes.addAll(createSubsequentNodes((Task) res.getTarget(), node, tasks, node.getLevel()));
			}

		}
		return nodes;
	}

	private static List<Nesteddependency> getNesteddependencies(Task max) {
		List<Nesteddependency> cLinks = new ArrayList<Nesteddependency>();
		for (Link link : max.getLinks()) {

			if (link instanceof Nesteddependency) {
				cLinks.add((Nesteddependency) link);
			}
		}
		return cLinks;
	}

	private static String getGroupOf(Resource res) {

		if (res instanceof Task) {
			Task task = (Task) res;
			if (task instanceof Loop) {
				return task.getWorkflowTaskState().getName() + "_loop";
			} else if (task instanceof Decision) {
				return task.getWorkflowTaskState().getName() + "_decision";
			} else {
				return task.getWorkflowTaskState().getName() + "_task";
			}
		}
		return "";
	}

	private static void setXPosition(List<Task> tasks, List<Node> nodes) {
		try {
			int xCount = 10;
			int layer = 1;
			for (Task taskWOdependencies : getInitialTasks(tasks)) {
				getNode(taskWOdependencies.getId(), nodes).setX(-30);
				positionSubElements(taskWOdependencies, nodes, -30);
				for (Task followingTask : getFollowingTasks(taskWOdependencies)) {
					rekSetX(followingTask, nodes, xCount, layer);
				}
			}
		} catch (RuntimeException e) {
			System.out.println("Could not set X position of Tasks!");
			System.out.println(e);
		}
	}

	private static void rekSetX(Task task, List<Node> nodes, int xCount, int xLayer) {
		if (task != null) {
			Node node = getNode(task.getId(), nodes);
			if (node != null) {
				int xPos = (xCount * xLayer) + node.getLevel();
				node.setX(xPos);
				if (getTaskdependencies(task).isEmpty()) {
					positionSubElements(task, nodes, xPos);
				} else {
					xLayer++;
					positionSubElements(task, nodes, xPos);
					for (Task followingTask : getFollowingTasks(task)) {
						if (isALoop(followingTask, task) == false) {
							rekSetX(followingTask, nodes, xCount, xLayer);
						} else {
							// node.setLevel(node.getLevel() + 1);
						}
					}
				}
			}
		}
	}

	private static boolean isALoop(Task followingTask, Task task) {
		for (Task connectedTask : getFollowingTasks(followingTask)) {
			if (followingTask.equals(task)) {
				return true;
			} else {
				return isALoop(connectedTask, task);
			}
		}
		return false;
	}

	private static void positionSubElements(Resource res, List<Node> nodes, int xCount) {
		for (Link link : res.getLinks()) {
			if (link.getTarget() != null && link.getTarget() instanceof Task == false) {
				Resource tar = link.getTarget();
				Node node = getNode(tar.getId(), nodes);
				node.setX(xCount);
				positionSubElements(tar, nodes, xCount);
			}
		}

	}

	private static List<Task> getFollowingTasks(Task task) {
		List<Task> followingTask = new ArrayList<Task>();
		for (Taskdependency dep : getTaskdependencies(task)) {
			followingTask.add((Task) dep.getTarget());
		}
		return followingTask;
	}

	private static List<Task> getInitialTasks(List<Task> tasks) {
		List<Task> initialTasks = new ArrayList<Task>();
		for (Task task : tasks) {
			for (Link rLink : task.getRlinks()) {
				if (rLink instanceof Taskdependency) {
					break;
				}
				initialTasks.add(task);
			}
			if (task.getRlinks() == null || task.getRlinks().isEmpty()) {
				initialTasks.add(task);
			}
		}
		// System.out.println(initialTasks);
		return initialTasks;
	}

	private static Node getNode(String id, List<Node> nodes) {
		for (Node node : nodes) {
			if (node.id.equals(id)) {
				return node;
			}
		}
		return null;
	}

	private static List<Taskdependency> getTaskdependencies(Task task) {
		List<Taskdependency> cLinks = new ArrayList<Taskdependency>();
		for (Link link : task.getLinks()) {

			if (link instanceof Taskdependency) {
				cLinks.add((Taskdependency) link);
			}
		}
		return cLinks;
	}

	private static Task getTaskWithMaxDependencies(List<Task> tasks) {
		Task max = tasks.get(0);
		int maxLinks = getTaskdependencies(max).size();
		for (Task task : tasks) {
			int taskLinks = getTaskdependencies(task).size();
			taskLinks += getNesteddependencies(task).size();
			if (taskLinks > maxLinks) {
				max = task;
				maxLinks = taskLinks;
			}
		}
		return max;
	}

	private static List<Node> createComponentNodes(List<Component> components) {
		List<Node> nodes = new ArrayList<Node>();
		int componentBaseLevel = 2;
		Component max = getComponentsWithMaxCompLinks(components);
		int maxLinks = getComponentLinks(max).size();
		componentBaseLevel += maxLinks - 1;
		Node node = null;

		while (components.isEmpty() == false) {
			max = getComponentsWithMaxCompLinks(components);
			maxLinks = getComponentLinks(max).size();
			node = createNode(max);
			node.setGroup(max.getOcciComponentState().getName() + "_component");
			node.setLevel(componentBaseLevel);
			nodes.add(node);

			List<Component> toRemove = new ArrayList<Component>();
			for (int i = 0; i < maxLinks; i++) {
				Component tarComp = (Component) getComponentLinks(max).get(i).getTarget();
				if (components.contains(tarComp)) {
					node = createNode(tarComp);
					node.setGroup(
							((Component) getComponentLinks(max).get(i).getTarget()).getOcciComponentState().getName()
									+ "_component");
					node.setLevel(componentBaseLevel - i);
					nodes.add(node);
					toRemove.add(tarComp);
				}
			}

			components.removeAll(toRemove);
			components.remove(max);

		}
		return nodes;
	}

	private static Component getComponentsWithMaxCompLinks(List<Component> components) {
		Component max = components.get(0);
		int maxLinks = getComponentLinks(max).size();
		for (Component comp : components) {
			int compLinks = getComponentLinks(comp).size();
			if (compLinks > maxLinks) {
				max = comp;
				maxLinks = compLinks;
			}
		}
		return max;
	}

	private static List<Componentlink> getComponentLinks(Component comp) {
		List<Componentlink> cLinks = new ArrayList<Componentlink>();
		for (Link link : comp.getLinks()) {
			if (link instanceof Componentlink) {
				cLinks.add((Componentlink) link);
			}
		}
		return cLinks;
	}

	private static List<AttributeState> getPartAttributes(Resource res) {
		List<AttributeState> list = new ArrayList<AttributeState>();
		for (MixinBase mixB : res.getParts()) {
			for (AttributeState attr : mixB.getAttributes()) {
				list.add(attr);
			}
		}
		return list;
	}

	private static String createAttrName(String name) {
		String str = name.replaceAll("([A-Z])", ".$1").toLowerCase();
		return str;
	}

	private static List<String> getParts(Resource res) {
		List<String> partList = new ArrayList<String>();
		for (MixinBase part : res.getParts()) {
			try {
				partList.add(part.getMixin().getScheme() + part.getMixin().getTerm());
			} catch (NullPointerException | InvalidValueException e) {
				partList.add("#Mixin for part could not be loaded: " + part.getMixin());
			}
		}
		return partList;
	}

	private static List<String> getParts(Link link) {
		List<String> partList = new ArrayList<String>();
		for (MixinBase part : link.getParts()) {
			try {
				partList.add(part.getMixin().getScheme() + part.getMixin().getTerm());
			} catch (NullPointerException | InvalidValueException e) {
				partList.add("#Mixin for part could not be loaded: " + part.getMixin());
			}
		}
		return partList;
	}

	private static List<String> getActions(Resource res) {
		// List<String> actList = new ArrayList<String>();
		// for(Action act: res.getKind().getActions()) {
		// actList.add(act.gnodeetScheme() + act.getTerm());
		// }
		return recActions(res.getKind(), new ArrayList<String>());
		// return actList;
	}

	private static List<String> recActions(Kind kind, List<String> actList) {
		if (kind.getScheme().equals("http://schemas.ogf.org/occi/core#")) {
			return actList;
		} else {
			for (Action act : kind.getActions()) {
				actList.add(act.getScheme() + act.getTerm());
			}
			return recActions(kind.getParent(), actList);
		}
	}

	private static List<String> getActions(Link link) {
		List<String> actList = new ArrayList<String>();
		for (Action act : link.getKind().getActions()) {
			actList.add(act.getScheme() + act.getTerm());

		}
		return actList;
	}

	public static ArrayList<Edge> createEdges(EList<org.eclipse.cmf.occi.core.Resource> resources) {
		ArrayList<Edge> edges = new ArrayList<Edge>();
		for (org.eclipse.cmf.occi.core.Resource res : EcoreUtil.copyAll(resources)) {
			for (Link link : res.getLinks()) {
				Edge edge = createEdge(link);

				if (link instanceof Networkinterface) {
					for (AttributeState attr : link.getAttributes()) {
						if (attr.getName().equals("occi.networkinterface.address")) {
							edge.setLabel(attr.getValue());
						}
					}
				} else if (link instanceof Datalink) {
					// TODO: Coloring of Datalink
					Datalink dLink = (Datalink) link;
					if (dLink.getTaskDatalinkState().getValue() == workflow.Status.SCHEDULED.getValue()) {
						edge.setColor(new EdgeColor("grey"));
					}
					if (dLink.getTaskDatalinkState().getValue() == workflow.Status.ACTIVE.getValue()) {
						edge.setColor(new EdgeColor("lightgreen"));
					}
					if (dLink.getTaskDatalinkState().getValue() == workflow.Status.FINISHED.getValue()) {
						edge.setColor(new EdgeColor("green"));
					}
					if (dLink.getTaskDatalinkState().getValue() == workflow.Status.INACTIVE.getValue()) {
						edge.setColor(new EdgeColor("blue"));
					}
					if (dLink.getTaskDatalinkState().getValue() == workflow.Status.ERROR.getValue()) {
						edge.setColor(new EdgeColor("red"));
					}
					edge.setDashes(true);
				} else if (link instanceof Taskdependency) {
					Taskdependency tLink = (Taskdependency) link;
					if (tLink.getTaskDependencyIsloose()) {
						edge.setLabel("loose");
					}
					for (MixinBase mixB : tLink.getParts()) {
						if (mixB instanceof Controlflowguard) {
							Controlflowguard cGuard = (Controlflowguard) mixB;
							if (cGuard.getControlflowGuard() != null
									&& cGuard.getControlflowGuard().equals("") == false) {
								edge.setLabel(cGuard.getControlflowGuard());
							}
						} else if (mixB.getMixin().getTerm().equals("controlflowguard")) {
							for (AttributeState attr : mixB.getAttributes()) {
								if (attr.getName().equals("controlflow.guard")) {
									edge.setLabel(attr.getValue());
								}
							}
							for (AttributeState attr : tLink.getAttributes()) {
								if (attr.getName().equals("controlflow.guard")) {
									edge.setLabel(attr.getValue());
								}
							}
						}
					}
				} else if (link instanceof Platformdependency) {
					edge.setDashes(true);
				} else if (link instanceof Executionlink) {
					edge.setDashes(true);
				}

				edges.add(edge);
			}
		}
		return edges;
	}

	private static Edge createEdge(Link link) {
		Edge edge = new Edge();
		edge.setId(link.getId());
		if (link.getSource() != null) {
			edge.setFrom(link.getSource().getId());
		}
		if (link.getTarget() != null) {
			edge.setTo(link.getTarget().getId());
		}

		edge.setAttributes(link.getAttributes());
		edge.setActions(getActions(link));
		edge.setParts(getParts(link));
		for (String str : edge.getParts()) {
			if (str.contains("#Mixin for part could not be loaded")) {
				EdgeColor color = new EdgeColor("red");
				edge.setColor(color);
				edge.setLabel("Error!");
			}
		}
		try {
			edge.setKind(link.getKind().getScheme() + link.getKind().getTerm());
		} catch (NullPointerException | InvalidValueException e) {
			EdgeColor color = new EdgeColor("red");
			edge.setColor(color);
			edge.setLabel("Error!");
			edge.setKind("Kind#Could not be loaded!");
		}

		for (EStructuralFeature feat : link.eClass().getEStructuralFeatures()) {
			if (link.eGet(feat) != null) {
				AttributeState attr = OCCIFactory.eINSTANCE.createAttributeState();
				attr.setName(createAttrName(feat.getName()));
				attr.setValue(link.eGet(feat).toString());

				AttributeState toSwitch = null;
				for (AttributeState registAttr : edge.getAttributes()) {
					if (registAttr.getName().equals(attr.getName())) {
						toSwitch = registAttr;
					}
				}
				edge.attributes.remove(toSwitch);
				edge.attributes.add(attr);
			}
		}
		return edge;

	}

	public static void comparisonColorNodes(org.eclipse.emf.ecore.resource.Resource designtimemodel,
			ArrayList<Node> designnodes, org.eclipse.emf.ecore.resource.Resource runmodel, ArrayList<Node> nodes) {
		Comparator comp = ComparatorFactory.getComparator("Simple", designtimemodel, runmodel);
		for (EObject obj : comp.getMissingElements()) {
			if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
				org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) obj;
				for (Node node : designnodes) {
					if (res.getId().equals(node.getId())) {
						node.setColor("lightgreen");
						node.setBordercolor("black");
					}
				}
			}

		}

		for (EObject obj : comp.getNewElements()) {
			if (obj instanceof org.eclipse.cmf.occi.core.Resource) {
				org.eclipse.cmf.occi.core.Resource res = (org.eclipse.cmf.occi.core.Resource) obj;
				for (Node node : nodes) {
					if (res.getId().equals(node.getId())) {
						node.setColor("red");
						node.setBordercolor("black");
					}
				}
			}

		}

	}
}
