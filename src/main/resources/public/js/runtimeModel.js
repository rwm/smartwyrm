function start() {
    drawDesigntime();
    drawInitialRuntime();
    // schedule the first invocation:
    //setTimeout(myPeriodicMethod, 5000);
}

function drawRuntimePopover(initialNetwork) {
    var runcontainer = document.getElementById('runtimepopover');
    if (popovernodes.length <= 0) {
        popovernodes.add(initialNetwork.nodes);
        popoveredges.add(initialNetwork.edges);
    }

    var options = getOptions();

    var data = {
        nodes: popovernodes,
        edges: popoveredges
    };

    popovernetwork = new vis.Network(runcontainer, data, options);
}


function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


function postNodeBuild(nodeBuild, entityType) {
    console.log("POST")
    console.log(nodeBuild)
    console.log(nodeBuild.attributes)
    $.post('builder/' + entityType, {entityBuild: JSON.stringify(nodeBuild)},
        function (returnedData) {
            runRedraw();
        });
}


function createNodeBuild(data, entityType) {
    nodeBuild.ext = nodeBuilder.ext.extension;
    nodeBuild.kind = nodeBuilder.kind.term;

    console.log("Node Builder:")
    console.log(nodeBuilder)

    for (i = 0; i < nodeBuilder.mixins.length; i++) {
        mixin = nodeBuilder.mixins[i]
        nodeBuild.mixins.push(mixin.scheme + mixin.term);
    }

    console.log("Adding attributes")
    if (entityType === "link") {
        let srcAttr = {
            name: "occi.core.source",
            value: "/" + nodes.get(data.from).kind.split("#")[1] + "/" + data.from + "/"
        }
        let tarAttr = {
            name: "occi.core.target",
            value: "/" + nodes.get(data.to).kind.split("#")[1] + "/" + data.to + "/"
        }
        console.log(srcAttr);
        console.log(nodeBuild.attributes);
        nodeBuild.attributes.push(srcAttr);
        console.log(nodeBuild.attributes);
        nodeBuild.attributes.push(tarAttr);
    }

    var attributeTextboxes = $(".attributestate");
    for (i = 0; i < attributeTextboxes.length; i++) {
        var attr = {
            name: attributeTextboxes[i].name,
            value: attributeTextboxes[i].value
        }
        nodeBuild.attributes.push(attr);
    }
    console.log("NodeBuild:")
    console.log(nodeBuild)
}

function checkIfAttributesCorrectlySet() {
    var attributeTextboxes = $(".attributestate");
    var valid = true;
    for (var i = 0; i < attributeTextboxes.length; i++) {
        var attr = attributeTextboxes[i];
        if (attr.required) {
            if (attr.value === "" || attr.value === null) {
                attr.classList.add("error");
                attr.outerHTML += "<div class='error'>This attribute is required!</div>"
                valid = false;
            }
        }
        if (attr.pattern !== "" && attr.pattern !== null) {

        }
    }
    return valid;
}

function parseMixinSelection(mixinSelection) {
    var mixinSet = []
    for (var i = 0; i < mixinSelection.length; i++) {
        mixin = JSON.parse(decodeURI(mixinSelection[i].value));
        mixinSet.push(mixin);
    }
    return mixinSet;
}

function entBuilderInit(data) {
    console.log(entityType)
    var wizard = $("#node-popUp").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        autoFocus: true,
        enableCancelButton: true,
        onStepChanged: function (event, currentIndex, newIndex) {
            var ext;
            var extSelection;
            var kind;
            var kindSelection;
            var mixin;
            var mixinSelection;
            var current = $("#node-popUp").steps("getCurrentIndex");
            if (current == 1) {
                $("#kindSelection").empty();
                extSelection = document.getElementById("extensionSelection").options[document.getElementById("extensionSelection").selectedIndex];
                ext = $.parseJSON(extSelection.value);
                nodeBuilder.ext = ext;
                getKinds(ext, entityType);
            }
            if (current == 2) {
                $("#mixinSelection").empty();
                kindSelection = document.getElementById("kindSelection").options[document.getElementById("kindSelection").selectedIndex];
                kind = $.parseJSON(kindSelection.value);
                nodeBuilder.kind = kind;
                getApplicableMixins(kind);
            }
            if (current == 3) {
                $("#attributeSelection").empty();
                mixinSelection = $("input[name='mixin']:checked")
                nodeBuilder.mixins = parseMixinSelection(mixinSelection);
                getOcciAttributes(nodeBuilder.kind, mixinSelection);
            }
        },
        onFinishing: function (event, currentIndex, newIndex) {
            if (checkIfAttributesCorrectlySet()) {
                createNodeBuild(selectedData, entityType);
                postNodeBuild(nodeBuild, entityType);
                resetNodeForm(currentIndex);
            } else {
                return false;
            }

        },
        onCanceled: function (event, currentIndex, newIndex) {
            resetNodeForm(currentIndex);
        }
    });
    return wizard;
}

function drawRuntime(initialNetwork) {

    var runcontainer = document.getElementById('runtime');
    nodes.add(initialNetwork.nodes);
    edges.add(initialNetwork.edges);


    keys.bind("delete", function (event) {
        console.log("Delete press detected!");
        deleteEntity();
    });


    var options = getOptions();

    options.manipulation = {
        addNode: function (data, callback) {
            entityType = "resource";
            // filling in the popup DOM elements
            if ($(".steps").length) {
                $("#node-popUp").show();
                return;
            }
            entBuilderInit(data);
            editNode(data, clearNodePopUp, callback);

        },
        addEdge: function (data, callback) {
            entityType = "link";
            selectedData = data;
            // filling in the popup DOM elements
            if ($(".steps").length) {
                $("#node-popUp").show();
                return;
            }
            entBuilderInit(data);
            editNode(data, clearNodePopUp, callback);
        },
        editNode: function (data, callback) {
            // filling in the popup DOM elements
            document.getElementById('node-operation').steps();
            editNode(data, cancelNodeEdit, callback);
        }
    }

    var data = {
        nodes: nodes,
        edges: edges
    };

    network = new vis.Network(runcontainer, data, options);


    network.on("deselectNode", function (params) {
        console.log('deselectNode Event:', params);
        w3_closeL(mySidebar);
        if (document.getElementById("editor") != null) {
            document.getElementById("editor").style.display = "none";
        }
        if (document.getElementById("designtime") != null) {
            document.getElementById("designtime").style.display = "initial";
        }
    });


    // network.on("stabilizationIterationsDone", function () {
    //    network.setOptions( { physics: false } );
    //});

    network.on("deselectEdge", function (params) {
        console.log('deselectEdge Event:', params);
        w3_closeL(mySidebar);
        if (document.getElementById("editor") != null) {
            document.getElementById("editor").style.display = "none";
        }
        if (document.getElementById("designtime") != null) {
            document.getElementById("designtime").style.display = "initial";
        }
    });

    network.on("doubleClick", function (properties) {
        var ids = properties.nodes;
        var clickedNodes = nodes.get(ids);
        getEditorContent(ids[0]);
        if (clickedNodes.length === 1) {
            var clickedNode = clickedNodes[0];
            if (network.isCluster(properties.nodes[0]) == true) {
                var clusterId = properties.nodes[0];
                var clusterNode = nodes.get(clusterId);
                var nodesInCluster = network.getNodesInCluster(clusterId);
                network.openCluster(properties.nodes[0]);
                network.stabilize();
                var rootNode = getClusterRootNode(clusterId);
                //network.moveNode(rootNode, 30,30);
            } else if (clickedNode.kind === "http://schemas.modmacao.org/occi/platform#application") {
                clusterApplication(clickedNode);
            } else if (clickedNode.kind === "http://schemas.ugoe.cs.rwm/monitoring#sensor") {
                clusterApplication(clickedNode);
            } else if (clickedNode.kind === "http://schemas.ugoe.cs.rwm/workflow#task") {
                clusterTask(clickedNode);
            } else if (clickedNode.kind === "http://schemas.ugoe.cs.rwm/workflow#loop") {
                clusterLoop(clickedNode);
            }
        }
    });

    network.on("selectNode", function (properties) {
        var clickedNode;
        w3_openL(mySidebar);
        if (network.isCluster(properties.nodes[0]) == true) {
            clickedNode = getClusterRootNode(properties.nodes[0]);
        } else {
            var ids = properties.nodes;
            var clickedNodes = nodes.get(ids);
            clickedNode = clickedNodes[0];
        }
        document.getElementById('mySidebarL').innerHTML = getAttributesForSidebar(clickedNode);
        document.getElementById('mySidebarL').innerHTML += getActionsForSidebar(clickedNode);
    });

    network.on("selectEdge", function (properties) {
        var ids = properties.edges;
        var clickedEdges = edges.get(ids);
        if (clickedEdges.length === 1) {
            var clickedEdge = clickedEdges[0];
            if (clickedEdge == null) {
                console.log("Clustered Edge selected");
            } else if (properties.nodes.length === 0) {
                w3_openL(mySidebar);
                document.getElementById('mySidebarL').innerHTML = getAttributesForSidebarEdge(clickedEdge);
                document.getElementById('mySidebarL').innerHTML += getActionsForSidebar(clickedEdge);
            }
        }
    });
}


function getClusterRootNode(clusterId) {
    console.log("Clustered Node selected!");
    console.log(clusterId);
    var containerId = clusterId.substring(8, clusterId.length);
    return nodes.get(containerId);
}

function clusterApplication(clickedNode) {
    network.stabilize();
    console.log(clickedNode);
    var clusterOptions = {
        joinCondition: function (parentOptions, childOptions) {
            if (childOptions.kind === "http://schemas.modmacao.org/occi/platform#component") {
                return true;
            } else if (childOptions.kind === "http://schemas.ugoe.cs.rwm/monitoring#resultprovider") {
                return true;
            } else if (childOptions.kind === "http://schemas.ugoe.cs.rwm/monitoring#datagatherer") {
                return true;
            } else if (childOptions.kind === "http://schemas.ugoe.cs.rwm/monitoring#dataprocessor") {
                return true;
            }

        },
        clusterNodeProperties: {
            allowSingleNodeCluster: true,
            border: clickedNode.border,
            label: clickedNode.label,
            size: 45,
            borderWidth: 3,
            color: {
                border: "#e7b544",
            },
            shape: 'image',
            id: "cluster:" + clickedNode.id,
            level: clickedNode.level,
            image: network.groups.get(clickedNode.group).image
        }
    };
    network.clusterByConnection(clickedNode.id, clusterOptions);
}

function hasMultipleIncomingEdges(node) {
    var incEdges = 0;
    for (let edgeId of network.getConnectedEdges(node.id)) {
        var edge = edges.get(edgeId);
        if (edge != null && edge.to === node.id) {
            incEdges++;
        }
    }
    return incEdges > 1;
}

function clusterTask(clickedNode) {
    network.stabilize();
    var clusterOptions = {
        joinCondition: function (parentOptions, childOptions) {
            console.log(childOptions);
            if (childOptions.kind === "http://schemas.modmacao.org/occi/platform#component") {
                return true;
            } else if (childOptions.kind === "http://schemas.modmacao.org/occi/platform#application"
                || childOptions.kind === "http://schemas.ugoe.cs.rwm/monitoring#sensor") {
                clusterApplication(childOptions);
                if (hasMultipleIncomingEdges(childOptions)) {
                    return false;
                } else {
                    return true;
                }
            } else if (network.isCluster(childOptions.id)) {
                var rootNode = getClusterRootNode(childOptions.id);
                if (rootNode.kind === "http://schemas.modmacao.org/occi/platform#application") {
                    console.log("Root " + rootNode);
                    if (hasMultipleIncomingEdges(rootNode)) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        },
        clusterNodeProperties: {
            allowSingleNodeCluster: true,
            border: clickedNode.border,
            label: clickedNode.label,
            size: 50,
            borderWidth: 3,
            id: "cluster:" + clickedNode.id,
            level: clickedNode.level,
            shape: 'image',
            image: network.groups.get(clickedNode.group).image,
            color: {
                background: network.groups.get(clickedNode.group).color.background,
                border: "#e7b544",
            },
        }
    };

    network.clusterByConnection(clickedNode.id, clusterOptions);
}

function buildsLoop(sourceTask, loop) {
    console.log("BUILDS LOOP:" + sourceTask.label);
    for (let edge of getRTdep(sourceTask)) {
        console.log(edge);
        console.log(edge.from);
        if (edge.from === loop.id) {
            return true;
        } else {
            buildsLoop(nodes.ged(edge.from), loop);
        }
    }
    return false;
}


function getRTdep(loop) {
    var rTdep = [];
    console.log("RTDEP:" + loop.label)
    for (let edgeId of network.getConnectedEdges(loop.id)) {
        var edge = edges.get(edgeId);
        if (edge != null && edge.kind === "http://schemas.ugoe.cs.rwm/workflow#controlflowlink") {
            if (edge.to === loop.id) {
                console.log("Adding: ");
                console.log(edge);
                rTdep.push(edge);
            }
        }
    }
    return rTdep;
}

function getLoopedTasks(loop) {
    var loopedTasks = [];
    console.log("Gathering looped tasks!")
    for (let rTdep of getRTdep(loop)) {
        var srcTask = nodes.get(rTdep.from);
        if (buildsLoop(srcTask, loop)) {
            console.log("Add looped task: ");
            console.log(srcTask);
            loopedTasks.push(srcTask);
        }
    }
    return loopedTasks;
}

function getIncomingEdges(node) {
    var incEdges = [];
    for (let edgeId of network.getConnectedEdges(node.id)) {
        var edge = edges.get(edgeId);
        if (edge != null && edge.to === node.id) {
            incEdges.push(edge);
        }
    }
    return incEdges;
}

function getTaskIds(loopedTasks) {
    var taskIds = []
    for (let task of loopedTasks) {
        taskIds.push(task.id);
    }
    return taskIds;
}

function isPartOfLoop(rootNode, loopedTasks, loop) {
    console.log("is part of loop: " + rootNode.label);
    if (hasMultipleIncomingEdges(rootNode)) {
        var incEdges = getIncomingEdges(rootNode);
        var taskIds = getTaskIds(loopedTasks);
        taskIds.push(loop.id);
        for (let incEdge of incEdges) {
            if (taskIds.includes(incEdge.from) === false) {
                return false;
            }
        }
    } else {
        return true;
    }
    return true;
}

function clusterLoop(clickedNode) {
    network.stabilize();
    console.log("CLICKED NODE")
    console.log(clickedNode);
    var loopedTasks = getLoopedTasks(clickedNode);
    var clusterOptions = {
        joinCondition: function (parentOptions, childOptions) {
            console.log(childOptions);
            if (childOptions.kind === "http://schemas.ugoe.cs.rwm/workflow#task"
                || childOptions.kind === "http://schemas.ugoe.cs.rwm/workflow#decision"
                || childOptions.kind === "http://schemas.ugoe.cs.rwm/workflow#loop") {
                for (let task of loopedTasks) {
                    if (childOptions.id === task.id) {
                        if (childOptions.kind === "http://schemas.ugoe.cs.rwm/workflow#task" ||
                            childOptions.kind === "http://schemas.ugoe.cs.rwm/workflow#decision") {
                            clusterTask(childOptions);
                        } else if (childOptions.kind === "http://schemas.ugoe.cs.rwm/workflow#loop") {
                            console.log("CLUSTERING LOOP: " + childOptions.id);
                            clusterLoop(childOptions);
                        }
                        return true;
                    }
                }
                for (let edgeId of network.getConnectedEdges(clickedNode.id)) {
                    var edge = edges.get(edgeId);
                    if (edge != null && edge.kind === "http://schemas.ugoe.cs.rwm/workflow#nesteddependency") {
                        if (childOptions.id === edge.to) {
                            console.log("CLUSTERING LOOP: " + childOptions.id);
                            clusterLoop(childOptions);
                            return true;
                        }
                    }
                }

            } else if (network.isCluster(childOptions.id)) {
                var rootNode = getClusterRootNode(childOptions.id);
                if (rootNode.kind === "http://schemas.ugoe.cs.rwm/workflow#task"
                    || rootNode.kind === "http://schemas.ugoe.cs.rwm/workflow#loop"
                    || rootNode.kind === "http://schemas.ugoe.cs.rwm/workflow#decision") {
                    for (let task of loopedTasks) {
                        if (task.id === rootNode.id) {
                            return true;
                        }
                    }

                    for (let edgeId of network.getConnectedEdges(clickedNode.id)) {
                        var edge = edges.get(edgeId);
                        if (edge != null && edge.kind === "http://schemas.ugoe.cs.rwm/workflow#nesteddependency") {
                            if (rootNode.id === edge.to) {
                                console.log("CLUSTERING LOOP: " + rootNode.id);
                                clusterLoop(rootNode);
                                return true;
                            }
                        }
                    }

                    return false;
                } else if (rootNode.kind === "http://schemas.modmacao.org/occi/platform#application"
                    || "http://schemas.ugoe.cs.rwm/monitoring#sensor") {
                    console.log("Root " + rootNode);
                    if (isPartOfLoop(rootNode, loopedTasks, clickedNode) === false) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        },
        clusterNodeProperties: {
            allowSingleNodeCluster: true,
            border: clickedNode.border,
            label: clickedNode.label,
            id: "cluster:" + clickedNode.id,
            level: clickedNode.level,
            shape: 'image',
            borderWidth: 3,
            size: 50,
            image: network.groups.get(clickedNode.group).image,
            color: {
                background: network.groups.get(clickedNode.group).color.background,
                border: "#e7b544",
            }
        }
    };

    network.clusterByConnection(clickedNode.id, clusterOptions);
}


function resetNodeForm(index) {
    nodeBuild.attributes.length = 0;
    nodeBuild.mixins.length = 0;
    nodeBuild.kind.length = 0;
    $("#networkInfo").empty();
    $("#kindSelection").empty();
    $("#mixinSelection").empty();
    $("#attributeSelection").empty();
    $("#node-popUp-t-0").click();
    $("#node-popUp").hide();
    $(".body:eq(" + index + ") label.error").remove();
    $(".body:eq(" + index + ") .error").removeClass("error");
    $(".steps").find('li').removeClass("error");
}

function deleteEntity() {
    if (clickedDelete()) {
        var id = network.getSelectedNodes();
        if (id.length < 1) {
            id = network.getSelectedEdges();
        }
        id = id[0];
        var alertId = processingAlert("alert-success", "Deleting entity with" + id + "!");
        $.post('delete', {entity: id},
            function (returnedData, status) {
                //newAlert(status, "Finished " + actionterm + " on " + str);
                fadeAlert(alertId);
                runRedraw();
            });
    }
}

function getEntity() {
    var id = network.getSelectedNodes();
    if (id.length < 1) {
        id = network.getSelectedEdges();
    }
    id = id[0];
    var alertId = processingAlert("alert-success", "Retrieving entity with" + id + "!");
    $.post('get', {entity: id},
        function (returnedData, status) {
            //newAlert(status, "Finished " + actionterm + " on " + str);
            fadeAlert(alertId);
            runRedraw();
        });

}

function clickedDelete() {
    if (confirm('Do you want to delete the selected Entity?')) {
        return true;
    } else {
        return false;
    }
}

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function copyStringToClipboard(str) {
    // Create new element
    var el = document.createElement('textarea');
    // Set value (string to be copied)
    el.value = str;
    // Set non-editable to avoid focus and move outside of view
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    // Select text inside element
    el.select();
    // Copy text to clipboard
    document.execCommand('copy');
    // Remove temporary element
    document.body.removeChild(el);
}

function processingAlert(type, message) {
    var uuid = uuidv4();
    $("#alert-area").append($("<div id=" + uuid + "><div class='loader'></div> <div class='alert-message " + type + "'><p> <center>" + message + "</center> </p> </div></div>"));
    return uuid;
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function fadeAlert(alertId) {
    //TODO: Fix fade only correct error message
    console.log("Fading alertID: " + alertId);
    //$(".alert-message").delay(2000).fadeOut("slow", function () {$(this).remove(); });
    $("#" + alertId).delay(2000).fadeOut("slow", function () {
        $(this).remove();
    });
    //$(".loader").delay(2000).fadeOut("slow", function () {$(this).remove(); });
}

function newAlert(type, message) {
    $("#alert-area").append($("<div class='alert-message " + type + " fade in' data-alert><p> <center>" + message + "</center> </p> </div>"));
    $(".alert-message").delay(2000).fadeOut("slow", function () {
        $(this).remove();
    });
}

function getEditorContent(id) {
    $.get('editorContent', {id: id},
        function (returnedData) {
            editor.setValue(returnedData.content, 1);
            editor.setTheme("ace/theme/textmate");
            editor.session.setMode("ace/mode/yaml");
            editor.setFontSize("18px");

            editor.commands.addCommand({
                name: 'save',
                bindKey: {win: "Ctrl-S", "mac": "Cmd-S"},
                exec: function (editor) {
                    $.post('editorContent', {script: returnedData.script, content: editor.getValue()},
                        function (returnedData) {
                            //console.log(returnedData);
                        });
                }
            })


            if (document.getElementById("editor") != null) {
                document.getElementById("editor").style.display = "initial";
            }
            if (document.getElementById("designtime") != null) {
                document.getElementById("designtime").style.display = "none";
            }
        });
}

function emptyRuntime() {
    console.log("Called Empty Runtime");
    $.post('empty',
        function (returnedData) {
            console.log(returnedData);
        });

}


function runRedraw() {
    $.ajax({
        url: "runtime",
        success: function (result) {
            updateRuntime(result);
        },
        complete: function () {
        }
    });
}

function periodicRuntimePopover() {
    $.ajax({
        url: "runtime",
        success: function (result) {
            console.log(result);
            updateRuntimePopover(result);
        },
        complete: function () {
            // schedule the next request *only* when the current one is complete:
            setTimeout(periodicRuntimePopover, 5000);
        }
    });
}


function myPeriodicMethod() {
    $.ajax({
        url: "runtime",
        success: function (result) {
            console.log(result);
            updateRuntime(result);
        },
        complete: function () {
            // schedule the next request *only* when the current one is complete:
            setTimeout(myPeriodicMethod, 5000);
        }
    });
}

function drawInitialRuntime() {
    $.ajax({
        url: "runtime",
        success: function (result) {
            drawRuntime(result);
        }
    });
}

function drawInitialRuntimePopover() {
    $.ajax({
        url: "runtime",
        success: function (result) {
            drawRuntimePopover(result);
        }
    });
}

function removeNodes(arrNodes, arrUpdNodes) {
    var changeDetected = false;
    for (var i = 0; i < arrNodes.length; i++) {
        var found = false;
        for (var k = 0; k < arrUpdNodes.length; k++) {
            if (arrUpdNodes[k].id == arrNodes[i]) {
                found = true;
                if (JSON.stringify(nodes.get(arrNodes[i])) !== JSON.stringify(arrUpdNodes[k])) {
                    console.log("Update Node: " + nodes.update(arrUpdNodes[k]));
                }
            }
        }
        if (found === false) {
            console.log("Removed Node: " + nodes.remove(arrNodes[i]));
            changeDetected = true;
        }
    }
    return changeDetected;
}

function addNodes(arrNodes, arrUpdNodes) {
    var changeDetected = false;
    for (var i = 0; i < arrUpdNodes.length; i++) {
        var found = false;
        for (var k = 0; k < arrNodes.length; k++) {
            if (arrNodes[k] == arrUpdNodes[i].id) {
                found = true;
                break;
            }
        }
        if (found === false) {
            nodes.add(arrUpdNodes[i]);
            changeDetected = true;
        }
    }
    return changeDetected;
}

function removeEdges(arrEdges, arrUpdEdges) {
    var changeDetected = false;
    for (var i = 0; i < arrEdges.length; i++) {
        var found = false;
        for (var k = 0; k < arrUpdEdges.length; k++) {
            if (arrUpdEdges[k].id == arrEdges[i]) {
                found = true;
                if (JSON.stringify(edges.get(arrEdges[i])) !== JSON.stringify(arrUpdEdges[k])) {
                    console.log("Update Edge: " + edges.update(arrUpdEdges[k]));
                }
            }
        }
        if (found === false) {
            console.log("Removed Edge: " + edges.remove(arrEdges[i]));
            changeDetected = true;
        }
    }
    return changeDetected;
}

function addEdges(arrEdges, arrUpdEdges) {
    var changeDetected = false;
    for (var i = 0; i < arrUpdEdges.length; i++) {
        var found = false;
        for (var k = 0; k < arrEdges.length; k++) {
            if (arrEdges[k] == arrUpdEdges[i].id) {
                found = true;
                break;
            }
        }
        if (found === false) {
            edges.add(arrUpdEdges[i]);
            changeDetected = true;
        }
    }
    return changeDetected;
}

function rocci(path) {
    console.log(path);
}

function updateRuntimePopover(updatedNetwork) {
    var arrNodes = popovernodes.getIds();
    var arrUpdNodes = updatedNetwork.nodes;
    removeNodes(arrNodes, arrUpdNodes);
    addNodes(arrNodes, arrUpdNodes);

    var arrEdges = popoveredges.getIds();
    var arrUpdEdges = updatedNetwork.edges;
    removeEdges(arrEdges, arrUpdEdges);
    addEdges(arrEdges, arrUpdEdges);
}

function getNetworkClusters() {
    var list = new Array();
    for (let node in network.body.nodes) {
        if (node.startsWith("cluster:")) {
            console.log(node);
            list.push(node);
        }
    }
    console.log(list);
    return list;
}

function updateClusteredNodes() {
    console.log(getNetworkClusters());
    for (let clusterId in getNetworkClusters()) {
        var cluster = getNetworkClusters()[clusterId];
        console.log(cluster)
        console.log("Updating cluster!");
        var clusterRootNode = getClusterRootNode(cluster);
        var rootGroup = network.groups.get(clusterRootNode.group);
        if (clusterRootNode !== null) {
            if (rootGroup.color !== null) {
                console.log("updateing color");
                console.log(rootGroup.color)
                network.clustering.updateClusteredNode(cluster, {color: rootGroup.color});
                network.clustering.updateClusteredNode(cluster, {color: {border: "#e7b544"}});
            }
            network.clustering.updateClusteredNode(cluster, {
                size: 50,
                image: network.groups.get(clusterRootNode.group).image
            });
        } else {
            console.log("Cluster root node is null for: " + cluster);
        }
    }
}

function updateRuntime(updatedNetwork) {
    var arrNodes = nodes.getIds();
    var arrUpdNodes = updatedNetwork.nodes;
    removeNodes(arrNodes, arrUpdNodes);
    addNodes(arrNodes, arrUpdNodes);

    var arrEdges = edges.getIds();
    var arrUpdEdges = updatedNetwork.edges;
    removeEdges(arrEdges, arrUpdEdges);
    addEdges(arrEdges, arrUpdEdges);
    updateClusteredNodes();
}

function editNode(data, cancelAction, callback) {
    //document.getElementById('node-popUp').style.display = 'block';
    document.getElementById('extensionSelection').options = getExtensions();
    $("#node-popUp").show();
}

function getKinds(extension, type) {
    let kindSelection;
    if (type == "resource") {
        kindSelection = extension.resourceKinds;
    } else {
        kindSelection = extension.linkKinds;
    }
    console.log(type)
    console.log(kindSelection)
    for (var i = 0; i < kindSelection.length; i++) {
        $("#kindSelection")
            .append($('<option />')  // Create new <option> element
                .val(JSON.stringify(kindSelection[i]))            // Set value as "Hello"
                .text(kindSelection[i].term)           // Set textContent as "Hello"
            );
    }
}

function getApplicableMixins(kind) {
    function mixinSection(applicableMixin) {
        str = ""
        str += '<div class=" mb-4 col-lg-3">'
        str += '<div class="card shadow">'
        str += '<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">\n' +
            '                  <h6 class="m-0 font-weight-bold text-primary">' + key + '</h6></div>'
        str += createMixinSelection(kind.applicableMixins[key])
        str += '</div>'
        str += '</div>'
        return str;
    }

    for (var key in kind.applicableMixins) {
        $("#mixinSelection")
            .append(mixinSection(kind.applicableMixins[key]))
    }
}

function createMixinSelection(mixins) {
    str = '<div class="card-body">';
    for (var i = 0; i < mixins.length; i++) {
        json = JSON.stringify(mixins[i]);
        encodedJson = encodeURI(json)
        str += '<label><input type="checkbox" value=' + encodedJson + ' name="mixin" /> ' + mixins[i].term + '</label><br />'
    }
    str += '</div>';
    return str;
}

function getOcciAttributes(kind, selectedMixins) {
    console.log(kind);
    let kindDivString = "attr_" + kind.scheme + kind.term;
    let div = document.getElementById(kindDivString);
    if (div === null) {
        div = document.createElement('div');
        div.id = kindDivString;
        div.classList.add("card");

        let header = document.createElement('div');
        header.classList = "card-header py-3 d-flex flex-row align-items-center justify-content-between";
        header.innerHTML = '<h6 class="m-0 font-weight-bold text-primary">' + kind.scheme + kind.term + '</h6>';
        div.append(header);

        let body = document.createElement('div');
        body.classList = "card-body";
        body.id = kindDivString + "-body";
        div.append(body);

        $("#attributeSelection")
            .append(div);
    }

    let body = document.getElementById(kindDivString + "-body")

    for (var i = 0; i < kind.attributes.length; i++) {
        var attr = kind.attributes[i];
        var newNode = document.createElement('div');
        newNode.innerHTML += label(attr);
        newNode.innerHTML += inputTextbox(attr);
        newNode.style = "margin: 10px; display: inline-block";
        body.append(newNode);
    }

    for (var j = 0; j < selectedMixins.length; j++) {

        var encodedJsonURI = selectedMixins[j].value;
        var decodedJsonURI = decodeURI(encodedJsonURI);
        var mixin = JSON.parse(decodedJsonURI);

        let mixDivString = "attr_" + mixin.scheme + mixin.term;
        let divMix = document.getElementById(mixDivString);
        if (divMix === null) {
            divMix = document.createElement('div');
            divMix.id = mixDivString;
            divMix.classList.add("card");

            let headerMix = document.createElement('div');
            headerMix.classList = "card-header py-3 d-flex flex-row align-items-center justify-content-between";
            headerMix.innerHTML = '<h6 class="m-0 font-weight-bold text-primary">' + mixin.scheme + mixin.term + '</h6>';
            divMix.append(headerMix);

            let bodyMix = document.createElement('div');
            bodyMix.classList = "card-body";
            bodyMix.id = mixDivString + "-body";
            divMix.append(bodyMix);

            $("#attributeSelection")
                .append(divMix);
        }

        let bodyMix = document.getElementById(mixDivString + "-body")

        for (var i = 0; i < mixin.attributes.length; i++) {
            var attr = mixin.attributes[i];
            var newNode = document.createElement('div');
            newNode.style = "margin: 10px; display: inline-block";
            newNode.innerHTML += label(attr);
            newNode.innerHTML += inputTextbox(attr);
            bodyMix.append(newNode)
        }
    }
}

function inputTextbox(attr) {
    console.log(attr)
    var str = "";
    str += inputHeader();
    str += attrValue(attr);
    str += attrName(attr);
    str += attrPattern(attr);
    str += attrRequired(attr);
    str += inputHeaderEnd();
    return str;
}

function label(attr) {
    if (attr.required) {
        return '<label for="' + attr.name + '">' + attr.name + '*</label>';
    } else {
        return '<label for="' + attr.name + '">' + attr.name + '</label>';
    }
}

function inputHeader() {
    return '<input class="attributestate" type="text"';
}

function attrRequired(attr) {
    if (attr.required) {
        return ' required ';
    } else {
        return "";
    }
}

function attrPattern(attr) {
    if (attr.pattern !== null && attr.pattern !== "") {
        return ' pattern="' + attr.pattern + '" ';
    } else {
        return "";
    }
}

function inputHeaderEnd() {
    return '/> <br />';
}

function attrValue(attr) {
    if (attr.default !== null && attr.default !== "") {
        return ' value="' + attr.default + '" ';
    } else {
        return ' value= "" ';
    }
}

function attrName(attr) {
    return 'name="' + attr.name + '"';
}

function getExtensions() {
    $.ajax({
        url: "builder/entity",
        success: function (result) {
            console.log(result);
            for (var i = 0; i < result.length; i++) {
                $("#extensionSelection")
                    .append($('<option />')  // Create new <option> element
                        .val(JSON.stringify(result[i]))            // Set value as "Hello"
                        .text(result[i].extension)           // Set textContent as "Hello"
                        .data({                  // Set multiple data-* attributes
                            extension: result[i]
                        })
                    );
            }
        }
    });
}

// Callback passed as parameter is ignored
function clearNodePopUp() {
    document.getElementById('node-saveButton').onclick = null;
    document.getElementById('node-nextButton').onclick = null;
    document.getElementById('node-cancelButton').onclick = null;
    document.getElementById('node-popUp').style.display = 'none';
}

function cancelNodeEdit(callback) {
    clearNodePopUp();
    callback(null);
}

function saveNodeData(data, callback) {
    alert(document.getElementById('extensionSelection').value);
    data.label = document.getElementById('node-label').value;
    clearNodePopUp();
    callback(data);
}


function getOptions() {
    var options = {
        interaction: {hover: true},
        //Allows editation
        manipulation: {
            enabled: true
        },
        "physics": {
            "enabled": true,
            "hierarchicalRepulsion": {
                "springLength": 20,
                "nodeDistance": 120,
                "damping": 0.15
            },
            "maxVelocity": 30,
            "minVelocity": 0.75,
            "solver": "hierarchicalRepulsion"
        },
        layout: {
            hierarchical: {
                enabled: true,
                levelSeparation: 150,
                nodeSpacing: 120,
                blockShifting: false,
                edgeMinimization: false,
                direction: 'DU',
                sortMethod: 'hubsize'
            }
        },
        nodes: {

            shape: 'dot',
            size: 35,
            font: {
                size: 20,
                color: 'black',
                strokeWidth: 8,
                strokeColor: 'white'
            },
            color: {
                border: 'black',
                background: 'white'
            },
            shapeProperties: {
                useBorderWithImage: true
            },
            scaling: {
                min: 10,
                max: 20,
            },
            borderWidth: 2
        },
        edges: {
            width: 2,
            color: {
                color: 'black',
                highlight: 'rgb(70,140,200)',
                hover: 'rgb(70,140,200)'
            },
            smooth: {
                type: 'dynamic',
                roundness: 0.4
            },
            font: {
                align: 'top',
                strokeWidth: 6,
                strokeColor: 'white'
            }
        },
        groups: {
            error_network: {
                shape: 'circularImage',
                image: '/img/network/error.png'
            },
            inactive_network: {
                shape: 'circularImage',
                image: '/img/network/inactive.png'
            },
            active_network: {
                shape: 'circularImage',
                image: '/img/network/active.png'
            },
            error_storage: {
                shape: 'circularImage',
                image: '/img/storage/error.png'
            },
            offline_storage: {
                shape: 'circularImage',
                image: '/img/storage/offline.png'
            },
            online_storage: {
                shape: 'circularImage',
                image: '/img/storage/online.png'
            },
            inactive_compute: {
                size: 45,
                shape: 'circularImage',
                image: '/img/compute/inactive.png'
            },
            active_compute: {
                size: 45,
                shape: 'circularImage',
                image: '/img/compute/active.png'
            },
            error_compute: {
                size: 45,
                shape: 'circularImage',
                image: '/img/compute/error.png'
            },
            suspended_compute: {
                size: 45,
                shape: 'circularImage',
                image: '/img/compute/suspended.png'
            },
            inactive_container: {
                shape: 'circularImage',
                image: '/img/container/inactive.png'
            },
            active_container: {
                shape: 'circularImage',
                image: '/img/container/active.png'
            },
            error_container: {
                shape: 'circularImage',
                image: '/img/container/error.png'
            },
            suspended_container: {
                shape: 'circularImage',
                image: '/img/container/suspended.png'
            },
            error_component: {
                shape: 'circularImage',
                image: '/img/component/error.png'
            },
            undeployed_component: {
                shape: 'circularImage',
                image: '/img/component/undeployed.png'
            },
            deployed_component: {
                shape: 'circularImage',
                image: '/img/component/deployed.png'
            },
            inactive_component: {
                shape: 'circularImage',
                image: '/img/component/inactive.png'
            },
            active_component: {
                shape: 'circularImage',
                image: '/img/component/active.png'
            },
            error_application: {
                shape: 'icon',
                shape: 'circularImage',
                image: '/img/application/error.png'
            },
            undeployed_application: {
                shape: 'circularImage',
                image: '/img/application/undeployed.png'
            },
            deployed_application: {
                shape: 'circularImage',
                image: '/img/application/deployed.png'
            },
            inactive_application: {
                shape: 'circularImage',
                image: '/img/application/inactive.png'
            },
            active_application: {
                shape: 'circularImage',
                image: '/img/application/active.png'
            },
            error_sensor: {
                shape: 'icon',
                shape: 'circularImage',
                image: '/img/sensor/error.png'
            },
            undeployed_sensor: {
                shape: 'circularImage',
                image: '/img/sensor/undeployed.png'
            },
            deployed_sensor: {
                shape: 'circularImage',
                image: '/img/sensor/deployed.png'
            },
            inactive_sensor: {
                shape: 'circularImage',
                image: '/img/sensor/inactive.png'
            },
            active_sensor: {
                shape: 'circularImage',
                image: '/img/sensor/active.png'
            },
            error_task: {
                shape: 'icon',
                shape: 'circularImage',
                size: 45,
                image: '/img/task/error.png',
                color: {
                    border: 'black',
                    background: 'white'
                },

            },
            scheduled_task: {
                shape: 'circularImage',
                image: '/img/task/scheduled.png',
                size: 45,
            },
            active_task: {
                shape: 'circularImage',
                image: '/img/task/active.png',
                size: 45,
            },
            finished_task: {
                shape: 'circularImage',
                size: 45,
                color: {
                    border: 'black',
                    background: 'green'
                },
                image: '/img/task/finished.png'
            },
            inactive_task: {
                shape: 'circularImage',
                image: '/img/task/inactive.png',
                size: 45,
            },
            skipped_task: {
                shape: 'circularImage',
                image: '/img/task/skipped.png',
                size: 45,
            },
            initialization_error: {
                shape: 'circularImage',
                image: '/img/initialization/error.png',
                color: {
                    border: 'black',
                    background: 'white'
                },
            },
            error_decision: {
                shape: 'diamond',
                size: 45,
                color: {
                    border: 'black',
                    background: 'red'
                },
            },
            scheduled_decision: {
                shape: 'diamond',
                size: 45,
                color: {
                    border: 'black',
                    background: 'grey'
                },
            },
            active_decision: {
                shape: 'diamond',
                size: 45,
                color: {
                    border: 'black',
                    background: 'lightgreen'
                },
            },
            finished_decision: {
                shape: 'diamond',
                size: 45,
                color: {
                    border: 'black',
                    background: 'green'
                },
            },
            inactive_decision: {
                shape: 'diamond',
                size: 45,
                color: {
                    border: 'black',
                    background: 'blue'
                },
            },
            skipped_decision: {
                shape: 'diamond',
                size: 45,
                color: {
                    border: 'black',
                    background: 'orange'
                },
            },
            error_loop: {
                shape: 'circularImage',
                image: '/img/loop/error.png',
                size: 45,
            },
            scheduled_loop: {
                shape: 'circularImage',
                image: '/img/loop/scheduled.png',
                size: 45,
            },
            active_loop: {
                shape: 'circularImage',
                image: '/img/loop/active.png',
                size: 45,
            },
            finished_loop: {
                shape: 'circularImage',
                image: '/img/loop/finished.png',
                size: 45,
                color: {
                    border: 'black',
                    background: 'green'
                },
            },
            inactive_loop: {
                shape: 'circularImage',
                image: '/img/loop/inactive.png',
                size: 45,
            },
            skipped_loop: {
                shape: 'circularImage',
                image: '/img/loop/skipped.png',
                size: 45,
            },
            default: {
                color: {
                    background: 'blue',
                    border: 'black'
                }
            }
        }
    };

    return options;
}
