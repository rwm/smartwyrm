package de.uoge.cs.rwm.smartwyrm;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.junit.Test;

import de.ugoe.cs.rwm.smartwyrm.controller.RocciController;

import java.io.IOException;

public class GitTest {

	@Test
	public void testHomeController() throws GitAPIException, IOException {
		RocciController rocci = new RocciController();
		rocci.importGit("https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.examples.git");
	}
}
