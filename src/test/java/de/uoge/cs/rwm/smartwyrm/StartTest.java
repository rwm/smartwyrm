package de.uoge.cs.rwm.smartwyrm;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.ugoe.cs.rwm.smartwyrm.controller.IndexController;

public class StartTest {

	@Test
	public void testHomeController() {
		IndexController homeController = new IndexController();
		String result = homeController.index();
		assertEquals(result, "index");
	}
}
